#ifndef MCM_VOICE_SERVICE_01_H
#define MCM_VOICE_SERVICE_01_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

#include <stdint.h>
#include "mcm_common_v01.h"


#ifdef __cplusplus
extern "C" {
#endif



/** @addtogroup mcm_voice_consts
    @{
  */
#define MCM_VOICE_STATUS_EV_V01 0x4002
#define MCM_VOICE_STAT_EV_V01 0x4003
#define MCM_VOICE_CONNECTION_EV_V01 0x4004
#define MCM_VOICE_DTMF_EV_V01 0x4005
#define MCM_VOICE_MUTE_EV_V01 0x4006

/**  GSM provides up to 8 calls, 3GPP2 just 2 */
#define MCM_VOICE_CALLS_MAX_V01 8
/**
    @}
  */

/** @addtogroup mcm_voice_enums
    @{
  */
typedef enum {
  MCM_VOICE_CALL_STATE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_VOICE_CALL_STATE_NONE_V01 = 0x0000, /**<  Place holder for zero value */
  MCM_VOICE_CALL_STATE_SETUP_V01 = 0x0001, /**<  dailing, incoming, cc setup */
  MCM_VOICE_CALL_STATE_ALERTING_V01 = 0x0002, /**<  MT call waiting, MO alterting */
  MCM_VOICE_CALL_STATE_ACTIVE_V01 = 0x0003, /**<  Call is active */
  MCM_VOICE_CALL_STATE_HOLD_V01 = 0x0004, /**<  Call placed on hold */
  MCM_VOICE_CALL_STATE_TERM_V01 = 0x0005, /**<  Call disconnected or terminating. */
  MCM_VOICE_CALL_STATE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_voice_call_state_t_v01;
/**
    @}
  */

/** @addtogroup mcm_voice_enums
    @{
  */
typedef enum {
  MCM_VOICE_CALL_TYPE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_VOICE_CALL_TYPE_NONE_V01 = 0x0000, /**<  Place holder for zero value */
  MCM_VOICE_CALL_TYPE_VOICE_V01 = 0x0001, /**<  Voice call */
  MCM_VOICE_CALL_TYPE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_voice_call_type_t_v01;
/**
    @}
  */

/** @addtogroup mcm_voice_enums
    @{
  */
typedef enum {
  MCM_VOICE_REASON_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_VOICE_REASON_NONE_V01 = 1, /**<  Place holder for zero value */
  MCM_VOICE_REASON_NORMAL_V01 = 2, /**<  Call ended normally */
  MCM_VOICE_REASON_BUSY_V01 = 3, /**<  call rejected */
  MCM_VOICE_REASON_CONGESTION_V01 = 4, /**<  network congestion */
  MCM_VOICE_REASON_CALL_BARRED_V01 = 5, /**<  incoming calls barred */
  MCM_VOICE_REASON_FDN_BLOCKED_V01 = 6, /**<  blocked by fix dialing */
  MCM_VOICE_REASON_DIAL_MODIFIED_TO_USSD_V01 = 7, /**<  Converted to USSD msg */
  MCM_VOICE_REASON_DIAL_MODIFIED_TO_SS_V01 = 8, /**<  Converted to Sup */
  MCM_VOICE_REASON_DIAL_MODIFIED_TO_DIAL_V01 = 9, /**<  Converted to other call */
  MCM_VOICE_REASON_ACM_LIMIT_EXCEEDED_V01 = 10, /**<  No funds */
  MCM_VOICE_REASON_CDMA_LOCKED_V01 = 11, /**<  locked until power cycle */
  MCM_VOICE_REASON_CDMA_FADE_V01 = 12, /**<  Call ended abnormally */
  MCM_VOICE_REASON_CDMA_INTERCEPT_V01 = 13, /**<  intercept from basestation */
  MCM_VOICE_REASON_CDMA_REORDER_V01 = 14, /**<  reorder from basestation */
  MCM_VOICE_REASON_CDMA_SO_REJECT_V01 = 15, /**<  release from basetation */
  MCM_VOICE_REASON_CDMA_RETRY_ORDER_V01 = 16, /**<  retry order, IS2000 */
  MCM_VOICE_REASON_CDMA_ACCESS_FAILURE_V01 = 17, /**<  other access failure */
  MCM_VOICE_REASON_CDMA_PREEMPTED_V01 = 18, /**<  incoming call from basestation */
  MCM_VOICE_REASON_CDMA_EMERGENCY_FLASHED_V01 = 19, /**<  Emergancy call flashed over this one */
  MCM_VOICE_REASON_CDMA_ACCESS_BLOCKED_V01 = 20, /**<  access blocked by basestation */
  MCM_VOICE_REASON_UNOBTAINABLE_NUMBER_V01 = 21, /**<  unassigned number */
  MCM_VOICE_REASON_IMSI_UNKNOWN_IN_HLR_V01 = 22, /**<  unknown IMSI */
  MCM_VOICE_REASON_IMSI_UNKNOWN_IN_VLR_V01 = 23, /**<  unknown IMSI */
  MCM_VOICE_REASON_IMEI_NOT_ACCEPTED_V01 = 24, /**<  unacceptable IMEI */
  MCM_VOICE_REASON_CALL_FAIL_ERROR_UNSPECIFIED_V01 = 0xFFFF, /**<  not specified */
  MCM_VOICE_REASON_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_voice_reason_t_v01;
/**
    @}
  */

/** @addtogroup mcm_voice_aggregates
    @{
  */
typedef struct {

  uint32_t type;
  /**<   UUS type, 0-6*/

  uint32_t dcs;
  /**<   UUS data coding scheme, 0-4*/

  uint32_t length;
  /**<   length of data, use to allocate client memory use
 mcm_voice_get_uusdata function to fetch a copy of the data*/
}mcm_voice_uusdata_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_voice_aggregates
    @{
  */
typedef struct {

  mcm_voice_call_type_t_v01 type;
  /**<   call type mcm_voice_call_types*/

  uint32_t domain;
  /**<   domain mcm_domain*/

  uint32_t tech;
  /**<   technology mcm_tech*/

  uint32_t flags;
  /**<   Call flags, use the following masks:
 0x01 -- 1 is MT, 0 is MO
 0x02 -- 1 is Conference (Mpty)
 0x04 -- 1 is CDMA Voice Privacy mode
 0x08 -- als, 0 is line 1, 1 is line 2
 0x30 -- number presentation 0 allowed, 1 restricted,
         2 unknown, 3 payphone.
 0xC0 -- name presentation; 0 allowed, 1 restricted,
         2 unknown, 3 payphone.
 0xFF80 -- reserved for future use
 Emergancy call is always call record 0.*/

  char forward[MCM_MAX_PHONE_NUMBER_V01 + 1];
  /**<   forwarding phone number in case of call forwarding*/
}mcm_voice_call_details_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_voice_aggregates
    @{
  */
typedef struct {

  uint32_t call_id;
  /**<   call id associated with this call*/

  mcm_voice_call_state_t_v01 state;
  /**<   current call state  mcm_voice_call_state*/

  char number[MCM_MAX_PHONE_NUMBER_V01 + 1];
  /**<   phone number*/

  mcm_voice_call_details_t_v01 details;
  /**<   extended config for a call*/

  mcm_voice_uusdata_t_v01 uusdata;
  /**<   user to user signaling data*/
}mcm_voice_call_record_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_voice_aggregates
    @{
  */
typedef struct {

  mcm_voice_call_details_t_v01 call_details;

  mcm_voice_uusdata_t_v01 uusdata;
}mcm_voice_config_parms_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_voice_aggregates
    @{
  */
typedef struct {

  mcm_voice_call_record_t_v01 calls[MCM_VOICE_CALLS_MAX_V01];
  /**<   list of calls, including pending calls*/

  mcm_voice_call_record_t_v01 emergancy;
  /**<   References the emergancy call in calls list,
 always call id (index) 0, if one exists. Otherwise null.*/
}mcm_voice_status_parms_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_voice_aggregates
    @{
  */
typedef struct {

  uint32_t num_calls;
  /**<   Number of valid call records*/

  uint32_t num_errors;
  /**<   Number of errors since last restart*/
}mcm_voice_stats_parms_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Configure the MCM voice interface. Configure the settings that define it. */
typedef struct {

  /* Mandatory */
  mcm_voice_config_parms_t_v01 config;
  /**<   Configuration data structure*/
}mcm_voice_config_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Configure the MCM voice interface. Configure the settings that define it. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_voice_config_resp_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_voice_get_config_req_msg_v01;

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Get the configuration status for this voice interface */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t config_valid;  /**< Must be set to true if config is being passed */
  mcm_voice_config_parms_t_v01 config;
  /**<   configuration status*/
}mcm_voice_get_config_resp_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_voice_get_status_req_msg_v01;

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Get the general status parameters associated with the module */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  mcm_voice_status_parms_t_v01 status;
  /**<   status of the connection*/
}mcm_voice_get_status_resp_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_voice_get_stats_req_msg_v01;

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Get the general statistics associated with the module */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t stats_valid;  /**< Must be set to true if stats is being passed */
  mcm_voice_stats_parms_t_v01 stats;
  /**<   statistics of the connection*/
}mcm_voice_get_stats_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Voice Connect */
typedef struct {

  /* Mandatory */
  char address[MCM_MAX_PHONE_NUMBER_V01 + 1];
  /**<   End point address of the connection to make*/

  /* Optional */
  uint8_t details_valid;  /**< Must be set to true if details is being passed */
  mcm_voice_call_details_t_v01 details;
  /**<   Connection (call) details, or null*/

  /* Optional */
  uint8_t uusdata_valid;  /**< Must be set to true if uusdata is being passed */
  mcm_voice_uusdata_t_v01 uusdata;
  /**<   Token id used to track this command, null ok.*/
}mcm_voice_connect_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Voice Connect */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t call_records_valid;  /**< Must be set to true if call_records is being passed */
  uint32_t call_records_len;  /**< Must be set to # of elements in call_records */
  mcm_voice_call_record_t_v01 call_records[MCM_VOICE_CALLS_MAX_V01];

  /* Optional */
  uint8_t emergency_call_id_valid;  /**< Must be set to true if emergency_call_id is being passed */
  uint8_t emergency_call_id;

  /* Optional */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  uint32_t status;

  /* Optional */
  uint8_t error_valid;  /**< Must be set to true if error is being passed */
  uint32_t error;

  /* Optional */
  uint8_t state_valid;  /**< Must be set to true if state is being passed */
  mcm_voice_call_state_t_v01 state;

  /* Optional */
  uint8_t reason_valid;  /**< Must be set to true if reason is being passed */
  mcm_voice_reason_t_v01 reason;
}mcm_voice_connect_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Disconnect or refuse the connection associated with id */
typedef struct {

  /* Mandatory */
  uint32_t call_id;
  /**<   call id of the connection to disconnect*/
}mcm_voice_disconnect_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Disconnect or refuse the connection associated with id */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  uint32_t status;

  /* Optional */
  uint8_t error_valid;  /**< Must be set to true if error is being passed */
  uint32_t error;

  /* Optional */
  uint8_t state_valid;  /**< Must be set to true if state is being passed */
  mcm_voice_call_state_t_v01 state;

  /* Optional */
  uint8_t call_id_valid;  /**< Must be set to true if call_id is being passed */
  uint32_t call_id;

  /* Optional */
  uint8_t reason_valid;  /**< Must be set to true if reason is being passed */
  mcm_voice_reason_t_v01 reason;
}mcm_voice_disconnect_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Update Connection */
typedef struct {

  /* Mandatory */
  uint32_t call_id;
  /**<   Call id to replace active, use 0 for none.*/

  /* Mandatory */
  mcm_voice_call_state_t_v01 new_state;
  /**<   new_state for active call, mcm_voice_call_state_t.*/
}mcm_voice_update_connection_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Update Connection */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  uint32_t status;

  /* Optional */
  uint8_t error_valid;  /**< Must be set to true if error is being passed */
  uint32_t error;

  /* Optional */
  uint8_t state_valid;  /**< Must be set to true if state is being passed */
  mcm_voice_call_state_t_v01 state;

  /* Optional */
  uint8_t call_id_valid;  /**< Must be set to true if call_id is being passed */
  uint32_t call_id;

  /* Optional */
  uint8_t reason_valid;  /**< Must be set to true if reason is being passed */
  mcm_voice_reason_t_v01 reason;
}mcm_voice_update_connection_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Switch Call state */
typedef struct {

  /* Mandatory */
  uint32_t call_id;
  /**<   call id to replace active, use 0 for none.*/

  /* Mandatory */
  mcm_voice_call_state_t_v01 new_state;
  /**<   new_state for active call, mcm_voice_call_state_t.*/
}mcm_voice_switch_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Switch Call state */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_voice_switch_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Get status associated with the connection id */
typedef struct {

  /* Mandatory */
  uint32_t call_id;
  /**<   call id of the connection to query*/
}mcm_voice_get_connection_status_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Get status associated with the connection id */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  mcm_voice_call_record_t_v01 status;
}mcm_voice_get_connection_status_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Sends a dtmf char dtmf over the connection id */
typedef struct {

  /* Mandatory */
  uint32_t call_id;
  /**<   call id of the connection to send the dtmf*/

  /* Mandatory */
  mcm_voice_call_state_t_v01 state;
  /**<   start or stop the dtmf signal, 1 start, 2 stop.*/

  /* Mandatory */
  char dtmf;
  /**<   DTMF character to be sent,
                           Valid DTMF characters are 0-9, A-D, '*', '#'.
                        */
}mcm_voice_dtmf_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Sends a dtmf char dtmf over the connection id */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_voice_dtmf_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Sends a dtmf sequence dtmf over the connection call_id */
typedef struct {

  /* Mandatory */
  uint32_t call_id;
  /**<   call id of the connection to send the dtmf*/

  /* Mandatory */
  char dtmf[252 + 1];
  /**<   DTMF character to be sent,
                           Valid DTMF characters are 0-9, A-D, '*', '#'.
                        */
}mcm_voice_dtmf_seq_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Sends a dtmf sequence dtmf over the connection call_id */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_voice_dtmf_seq_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Mute/Unmute a voice call */
typedef struct {

  /* Mandatory */
  uint32_t call_id;
  /**<   call id of the connection to Mute/Unmute*/

  /* Mandatory */
  mcm_voice_call_state_t_v01 state;
  /**<   mute or unmute, 1 - mute, 2 - unmute*/
}mcm_voice_mute_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Mute/Unmute a voice call */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_voice_mute_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Get uusdata */
typedef struct {

  /* Mandatory */
  uint32_t call_id;
  /**<   call id of the connection to get the data*/

  /* Mandatory */
  uint32_t mem_len;  /**< Must be set to # of elements in mem */
  char mem[252];
  /**<   memory allocated for uusdata, must be >= len*/
}mcm_voice_get_uusdata_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Get uusdata */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_voice_get_uusdata_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Sends a flash sequence char flash over the connection call_id */
typedef struct {

  /* Mandatory */
  uint32_t call_id;
  /**<   call id of the connection*/

  /* Mandatory */
  char flash[82 + 1];
  /**<   A null terminated FLASH string to be sent, 82 chars max*/
}mcm_voice_flash_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Sends a flash sequence char flash over the connection call_id */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_voice_flash_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Voice dial command */
typedef struct {

  /* Mandatory */
  char phone_number[MCM_MAX_PHONE_NUMBER_V01 + 1];
  /**<   phone number to dial, the connection address.*/
}mcm_voice_dial_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Voice dial command */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t call_id_valid;  /**< Must be set to true if call_id is being passed */
  uint32_t call_id;

  /* Optional */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  uint32_t status;

  /* Optional */
  uint8_t error_valid;  /**< Must be set to true if error is being passed */
  uint32_t error;

  /* Optional */
  uint8_t state_valid;  /**< Must be set to true if state is being passed */
  mcm_voice_call_state_t_v01 state;

  /* Optional */
  uint8_t reason_valid;  /**< Must be set to true if reason is being passed */
  mcm_voice_reason_t_v01 reason;
}mcm_voice_dial_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Voice answer command */
typedef struct {

  /* Mandatory */
  uint32_t call_id;
  /**<   call id associated with the connection.*/
}mcm_voice_answer_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Voice answer command */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  uint32_t status;

  /* Optional */
  uint8_t error_valid;  /**< Must be set to true if error is being passed */
  uint32_t error;

  /* Optional */
  uint8_t state_valid;  /**< Must be set to true if state is being passed */
  mcm_voice_call_state_t_v01 state;

  /* Optional */
  uint8_t call_id_valid;  /**< Must be set to true if call_id is being passed */
  uint32_t call_id;

  /* Optional */
  uint8_t reason_valid;  /**< Must be set to true if reason is being passed */
  mcm_voice_reason_t_v01 reason;
}mcm_voice_answer_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Voice answer command */
typedef struct {

  /* Mandatory */
  uint32_t call_id;
  /**<   call id associated with the connection.*/
}mcm_voice_hangup_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Voice answer command */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  uint32_t status;

  /* Optional */
  uint8_t error_valid;  /**< Must be set to true if error is being passed */
  uint32_t error;

  /* Optional */
  uint8_t state_valid;  /**< Must be set to true if state is being passed */
  mcm_voice_call_state_t_v01 state;

  /* Optional */
  uint8_t call_id_valid;  /**< Must be set to true if call_id is being passed */
  uint32_t call_id;

  /* Optional */
  uint8_t reason_valid;  /**< Must be set to true if reason is being passed */
  mcm_voice_reason_t_v01 reason;
}mcm_voice_hangup_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Request Message; Register for Indication of Events */
typedef struct {

  /* Optional */
  uint8_t register_status_event_valid;  /**< Must be set to true if register_status_event is being passed */
  uint8_t register_status_event;
  /**<    - MCM_VOICE_STATUS_EV*/

  /* Optional */
  uint8_t register_stat_event_valid;  /**< Must be set to true if register_stat_event is being passed */
  uint8_t register_stat_event;
  /**<   - MCM_VOICE_STAT_EV*/

  /* Optional */
  uint8_t register_connection_event_valid;  /**< Must be set to true if register_connection_event is being passed */
  uint8_t register_connection_event;
  /**<   - MCM_VOICE_CONNECTION_EV*/

  /* Optional */
  uint8_t register_dtmf_event_valid;  /**< Must be set to true if register_dtmf_event is being passed */
  uint8_t register_dtmf_event;
  /**<   - MCM_VOICE_DTMF_EV*/

  /* Optional */
  uint8_t register_mute_event_valid;  /**< Must be set to true if register_mute_event is being passed */
  uint8_t register_mute_event;
  /**<   - MCM_VOICE_MUTE_EV*/
}mcm_voice_event_register_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Response Message; Register for Indication of Events */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_voice_event_register_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_voice_messages
    @{
  */
/** Indication Message; Indication for MCM_VOICE_STATUS_EV */
typedef struct {

  /* Optional */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  uint32_t status;
}mcm_voice_status_ind_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_voice_stat_ind_msg_v01;

/** @addtogroup mcm_voice_messages
    @{
  */
/** Indication Message; Indication for MCM_VOICE_CONNECTION_EV */
typedef struct {

  /* Optional */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  uint32_t status;
  /**<   Current status of command  mcm_cmd_state,
 MCM_CMD_STATE_NONE when not associated directly with
 a command, eg. an incoming MT call.*/

  /* Optional */
  uint8_t error_valid;  /**< Must be set to true if error is being passed */
  uint32_t error;
  /**<   An  mcm_error error code assicitated with this command
 when state is  MCM_CMD_STATE_ERROR.*/

  /* Optional */
  uint8_t state_valid;  /**< Must be set to true if state is being passed */
  mcm_voice_call_state_t_v01 state;
  /**<   current call state  mcm_voice_call_state*/

  /* Optional */
  uint8_t call_id_valid;  /**< Must be set to true if call_id is being passed */
  uint32_t call_id;
  /**<   call id, assigned once call has been placed by the system.*/

  /* Optional */
  uint8_t reason_valid;  /**< Must be set to true if reason is being passed */
  mcm_voice_reason_t_v01 reason;
  /**<   reason for call disconnected  mcm_voice_reason,
 only valid when state is MCM_VOICE_CALL_STATE_TERM*/
}mcm_voice_connection_ind_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_voice_dtmf_ind_msg_v01;

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_voice_mute_ind_msg_v01;

/*Service Message Definition*/
/** @addtogroup mcm_voice_msg_ids
    @{
  */
#define MCM_VOICE_CONFIG_REQ_V01 0x1000
#define MCM_VOICE_CONFIG_RESP_V01 0x1000
#define MCM_VOICE_GET_CONFIG_REQ_V01 0x1001
#define MCM_VOICE_GET_CONFIG_RESP_V01 0x1001
#define MCM_VOICE_GET_STATUS_REQ_V01 0x1002
#define MCM_VOICE_GET_STATUS_RESP_V01 0x1002
#define MCM_VOICE_GET_STATS_REQ_V01 0x1003
#define MCM_VOICE_GET_STATS_RESP_V01 0x1003
#define MCM_VOICE_CONNECT_REQ_V01 0x1004
#define MCM_VOICE_CONNECT_RESP_V01 0x1004
#define MCM_VOICE_DISCONNECT_REQ_V01 0x1005
#define MCM_VOICE_DISCONNECT_RESP_V01 0x1005
#define MCM_VOICE_UPDATE_CONNECTION_REQ_V01 0x1006
#define MCM_VOICE_UPDATE_CONNECTION_RES_V01 0x1006
#define MCM_VOICE_SWITCH_REQ_V01 0x1007
#define MCM_VOICE_SWITCH_RESP_V01 0x1007
#define MCM_VOICE_GET_CONNECTION_STATUS_REQ_V01 0x1008
#define MCM_VOICE_GET_CONNECTION_STATUS_RESP_V01 0x1008
#define MCM_VOICE_DTMF_REQ_V01 0x1009
#define MCM_VOICE_DTMF_RESP_V01 0x1009
#define MCM_VOICE_DTMF_SEQ_REQ_V01 0x100A
#define MCM_VOICE_DTMF_SEQ_RESP_V01 0x100A
#define MCM_VOICE_MUTE_REQ_V01 0x100B
#define MCM_VOICE_MUTE_RESP_V01 0x100B
#define MCM_VOICE_GET_UUSDATA_REQ_V01 0x100C
#define MCM_VOICE_GET_UUSDATA_RESP_V01 0x100C
#define MCM_VOICE_FLASH_REQ_V01 0x100D
#define MCM_VOICE_FLASH_RESP_V01 0x100D
#define MCM_VOICE_DIAL_REQ_V01 0x100E
#define MCM_VOICE_DIAL_RESP_V01 0x100E
#define MCM_VOICE_ANSWER_REQ_V01 0x100F
#define MCM_VOICE_ANSWER_RESP_V01 0x100F
#define MCM_VOICE_HANGUP_REQ_V01 0x1010
#define MCM_VOICE_HANGUP_RESP_V01 0x1010
#define MCM_VOICE_EVENT_REGISTER_REQ_V01 0x1011
#define MCM_VOICE_EVENT_REGISTER_RESP_V01 0x1011
#define MCM_VOICE_STATUS_IND_V01 0x1012
#define MCM_VOICE_STAT_IND_V01 0x1013
#define MCM_VOICE_CONNECTION_IND_V01 0x1014
#define MCM_VOICE_DTMF_IND_V01 0x1015
#define MCM_VOICE_MUTE_IND_V01 0x1016
/**
    @}
  */

#ifdef __cplusplus
}
#endif
#endif

