#ifndef MCM_UIM_SERVICE_01_H
#define MCM_UIM_SERVICE_01_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


#include <stdint.h>
#include "mcm_common_v01.h"


#ifdef __cplusplus
extern "C" {
#endif

/** @addtogroup mcm_uim_consts
    @{
  */
#define MCM_SIM_CARD_STATUS_EV_V01 0x2012
#define MCM_SIM_REFRESH_EVENT_V01 0x2011
#define MCM_UIM_IMSI_LEN_V01 17
#define MCM_UIM_ICCID_LEN_V01 21
#define MCM_UIM_NUM_PLMN_MAX_V01 24
#define MCM_UIM_PHONE_NUM_MAX_V01 21
#define MCM_UIM_CHAR_PATH_MAX_V01 21
#define MCM_UIM_DATA_MAX_V01 4096
#define MCM_UIM_PIN_MAX_V01 9
#define MCM_UIM_MAX_NUM_CARDS_V01 5
#define MCM_UIM_MAX_REFRESH_FILES_V01 100
#define MCM_UIM_MAX_BINARY_PATHS_V01 10
#define MCM_UIM_CK_MAX_V01 17
#define MCM_UIM_MCC_LEN_V01 3
#define MCM_UIM_MNC_MAX_V01 3
/**
    @}
  */

/** @addtogroup mcm_uim_enums
    @{
  */
typedef enum {
  MCM_UIM_SLOT_ID_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_UIM_SLOT_ID_1_V01 = 0xB01,
  MCM_UIM_SLOT_ID_2_V01 = 0xB02,
  MCM_UIM_SLOT_ID_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_uim_slot_id_t_v01;
/**
    @}
  */

/** @addtogroup mcm_uim_enums
    @{
  */
typedef enum {
  MCM_UIM_APP_TYPE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_UIM_APP_TYPE_UNKNOWN_V01 = 0xB00,
  MCM_UIM_APP_TYPE_3GPP_V01 = 0xB01,
  MCM_UIM_APP_TYPE_3GPP2_V01 = 0xB02,
  MCM_UIM_APP_TYPE_ISIM_V01 = 0xB03,
  MCM_UIM_APP_TYPE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_uim_app_type_t_v01;
/**
    @}
  */

/** @addtogroup mcm_uim_enums
    @{
  */
typedef enum {
  MCM_UIM_FILE_TYPE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_UIM_FILE_TYPE_UNKNOWN_V01 = 0xB00,
  MCM_UIM_FILE_TYPE_TRANSPARENT_V01 = 0xB01,
  MCM_UIM_FILE_TYPE_CYCLIC_V01 = 0xB02,
  MCM_UIM_FILE_TYPE_LINEAR_FIXED_V01 = 0xB03,
  MCM_UIM_FILE_TYPE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_uim_file_type_t_v01;
/**
    @}
  */

/** @addtogroup mcm_uim_enums
    @{
  */
typedef enum {
  MCM_UIM_PIN_ID_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_UIM_PIN_ID_1_V01 = 0xB01,
  MCM_UIM_PIN_ID_2_V01 = 0xB02,
  MCM_UIM_PIN_ID_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_uim_pin_id_t_v01;
/**
    @}
  */

/** @addtogroup mcm_uim_enums
    @{
  */
typedef enum {
  MCM_UIM_PIN_OPERATION_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_UIM_PIN_OPERATION_DISABLE_V01 = 0xB00,
  MCM_UIM_PIN_OPERATION_ENABLE_V01 = 0xB01,
  MCM_UIM_PIN_OPERATION_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_uim_pin_operation_t_v01;
/**
    @}
  */

/** @addtogroup mcm_uim_enums
    @{
  */
typedef enum {
  MCM_UIM_PERSO_FEATURE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_UIM_PERSO_FEATURE_STATUS_UNKNOWN_V01 = 0xB00,
  MCM_UIM_PERSO_FEATURE_STATUS_3GPP_NETWORK_V01 = 0xB01,
  MCM_UIM_PERSO_FEATURE_STATUS_3GPP_NETWORK_SUBSET_V01 = 0xB02,
  MCM_UIM_PERSO_FEATURE_STATUS_3GPP_SERVICE_PROVIDER_V01 = 0xB03,
  MCM_UIM_PERSO_FEATURE_STATUS_3GPP_CORPORATE_V01 = 0xB04,
  MCM_UIM_PERSO_FEATURE_STATUS_3GPP_SIM_V01 = 0xB05,
  MCM_UIM_PERSO_FEATURE_STATUS_3GPP2_NETWORK_TYPE_1_V01 = 0xB06,
  MCM_UIM_PERSO_FEATURE_STATUS_3GPP2_NETWORK_TYPE_2_V01 = 0xB07,
  MCM_UIM_PERSO_FEATURE_STATUS_3GPP2_HRPD_V01 = 0xB08,
  MCM_UIM_PERSO_FEATURE_STATUS_3GPP2_SERVICE_PROVIDER_V01 = 0xB09,
  MCM_UIM_PERSO_FEATURE_STATUS_3GPP2_CORPORATE_V01 = 0xB0A,
  MCM_UIM_PERSO_FEATURE_STATUS_3GPP2_RUIM_V01 = 0xB0B,
  MCM_UIM_PERSO_FEATURE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_uim_perso_feature_t_v01;
/**
    @}
  */

/** @addtogroup mcm_uim_enums
    @{
  */
typedef enum {
  MCM_UIM_PERSO_OPERATION_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_UIM_PERSO_OPERATION_DEACTIVATE_V01 = 0xB00,
  MCM_UIM_PERSO_OPERATION_UNBLOCK_V01 = 0xB01,
  MCM_UIM_PERSO_OPERATION_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_uim_perso_operation_t_v01;
/**
    @}
  */

/** @addtogroup mcm_uim_enums
    @{
  */
typedef enum {
  MCM_UIM_CARD_T_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_UIM_CARD_TYPE_UNKNOWN_V01 = 0xB00,
  MCM_UIM_CARD_TYPE_ICC_V01 = 0xB01,
  MCM_UIM_CARD_TYPE_UICC_V01 = 0xB02,
  MCM_UIM_CARD_T_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_uim_card_t_t_v01;
/**
    @}
  */

/** @addtogroup mcm_uim_enums
    @{
  */
typedef enum {
  MCM_UIM_SUBSCRIPTION_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_UIM_PROV_STATE_NONE_V01 = 0xB00,
  MCM_UIM_PROV_STATE_PRI_V01 = 0xB01,
  MCM_UIM_PROV_STATE_SEC_V01 = 0xB02,
  MCM_UIM_PROV_STATE_TER_V01 = 0xB03,
  MCM_UIM_SUBSCRIPTION_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_uim_subscription_t_v01;
/**
    @}
  */

/** @addtogroup mcm_uim_enums
    @{
  */
typedef enum {
  MCM_UIM_APP_STATE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_UIM_APP_STATE_UNKNOWN_V01 = 0xB00,
  MCM_UIM_APP_STATE_DETECTED_V01 = 0xB01,
  MCM_UIM_APP_STATE_PIN1_REQ_V01 = 0xB02,
  MCM_UIM_APP_STATE_PUK1_REQ_V01 = 0xB03,
  MCM_UIM_APP_STATE_INITALIZATING_V01 = 0xB04,
  MCM_UIM_APP_STATE_PERSO_CK_REQ_V01 = 0xB05,
  MCM_UIM_APP_STATE_PERSO_PUK_REQ_V01 = 0xB06,
  MCM_UIM_APP_STATE_PERSO_PERMANENTLY_BLOCKED_V01 = 0xB07,
  MCM_UIM_APP_STATE_PIN1_PERM_BLOCKED_V01 = 0xB08,
  MCM_UIM_APP_STATE_ILLEGAL_V01 = 0xB09,
  MCM_UIM_APP_STATE_READY_V01 = 0xB0A,
  MCM_UIM_APP_STATE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_uim_app_state_t_v01;
/**
    @}
  */

/** @addtogroup mcm_uim_enums
    @{
  */
typedef enum {
  MCM_UIM_PIN_STATE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_UIM_PIN_STATE_UNKNOWN_V01 = 0xB01,
  MCM_UIM_PIN_STATE_ENABLED_NOT_VERIFIED_V01 = 0xB02,
  MCM_UIM_PIN_STATE_ENABLED_VERIFIED_V01 = 0xB03,
  MCM_UIM_PIN_STATE_DISABLED_V01 = 0xB04,
  MCM_UIM_PIN_STATE_BLOCKED_V01 = 0xB05,
  MCM_UIM_PIN_STATE_PERMANENTLY_BLOCKED_V01 = 0xB06,
  MCM_UIM_PIN_STATE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_uim_pin_state_t_v01;
/**
    @}
  */

/** @addtogroup mcm_uim_enums
    @{
  */
typedef enum {
  MCM_UIM_CARD_STATE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_UIM_CARD_STATE_UNKNOWN_V01 = 0xB01,
  MCM_UIM_CARD_STATE_ABSENT_V01 = 0xB02,
  MCM_UIM_CARD_STATE_PRESENT_V01 = 0xB03,
  MCM_UIM_CARD_STATE_ERROR_UNKNOWN_V01 = 0xB04,
  MCM_UIM_CARD_STATE_ERROR_POWER_DOWN_V01 = 0xB05,
  MCM_UIM_CARD_STATE_ERROR_POLL_ERROR_V01 = 0xB06,
  MCM_UIM_CARD_STATE_ERROR_NO_ATR_RECEIVED_V01 = 0xB07,
  MCM_UIM_CARD_STATE_ERROR_VOLT_MISMATCH_V01 = 0xB08,
  MCM_UIM_CARD_STATE_ERROR_PARITY_ERROR_V01 = 0xB09,
  MCM_UIM_CARD_STATE_ERROR_SIM_TECHNICAL_PROBLEMS_V01 = 0xB0A,
  MCM_UIM_CARD_STATE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_uim_card_state_t_v01;
/**
    @}
  */

/** @addtogroup mcm_uim_enums
    @{
  */
typedef enum {
  MCM_UIM_REFRESH_MODE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_UIM_REFRESH_RESET_V01 = 0xB01,
  MCM_UIM_REFRESH_NAA_INIT_V01 = 0xB02,
  MCM_UIM_REFRESH_NAA_FCN_V01 = 0xB03,
  MCM_UIM_REFRESH_NAA_INIT_FCN_V01 = 0xB04,
  MCM_UIM_REFRESH_NAA_INIT_FULL_FCN_V01 = 0xB05,
  MCM_UIM_REFRESH_NAA_APP_RESET_V01 = 0xB06,
  MCM_UIM_REFRESH_3G_SESSION_RESET_V01 = 0xB07,
  MCM_UIM_REFRESH_MODE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_uim_refresh_mode_t_v01;
/**
    @}
  */

/** @addtogroup mcm_uim_aggregates
    @{
  */
typedef struct {

  mcm_uim_slot_id_t_v01 slot_id;
  /**<   Indicates the slot to be used. Valid values:
        - 1 -- Slot 1
        - 2 -- Slot 2
  */

  /*  application type */
  mcm_uim_app_type_t_v01 app_t;
  /**<   Indicates the type of the application. Valid values:
        - 0 -- Unknown
        - 1 -- 3GPP application
        - 2 -- 3GPP2 application
        - 3 -- ISIM application
       Other values are reserved for the future and are to be handled as
       ``Unknown''.
  */
}mcm_uim_application_identification_info_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_uim_aggregates
    @{
  */
typedef struct {

  char mcc[MCM_UIM_MCC_LEN_V01];
  /**<   MCC value in ASCII characters.*/

  uint32_t mnc_len;  /**< Must be set to # of elements in mnc */
  char mnc[MCM_UIM_MNC_MAX_V01];
  /**<   MNC value in ASCII characters.*/
}mcm_uim_plmn_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_uim_aggregates
    @{
  */
typedef struct {

  uint16_t offset;

  uint8_t record_num;

  uint16_t length;

  char path[MCM_UIM_CHAR_PATH_MAX_V01];
  /**<   File path in ASCII characters.*/
}mcm_uim_file_access_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_uim_aggregates
    @{
  */
typedef struct {

  uint8_t sw1;
  /**<   SW1 received from the card.*/

  uint8_t sw2;
  /**<   SW2 received from the card.*/
}mcm_uim_card_result_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_uim_aggregates
    @{
  */
typedef struct {

  mcm_uim_file_type_t_v01 file_t;

  uint16_t size;

  uint16_t record_count;
}mcm_uim_file_info_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_uim_aggregates
    @{
  */
typedef struct {

  mcm_uim_perso_feature_t_v01 feature;
  /**<   Indicates the personalization feature to deactivate or unblock.
       Valid values:
         - 0 -- GW network personalization
         - 1 -- GW network subset personalization
         - 2 -- GW service provider personalization
         - 3 -- GW corporate personalization
         - 4 -- GW UIM personalization
         - 5 -- 1X network type 1 personalization
         - 6 -- 1X network type 2 personalization
         - 7 -- 1X HRPD personalization
         - 8 -- 1X service provider personalization
         - 9 -- 1X corporate personalization
         - 10 -- 1X RUIM personalization
  */

  mcm_uim_perso_operation_t_v01 operation;
  /**<   Indicates the operation to perform. Valid values:
        - 0 -- Deactivate personalization.
        - 1 -- Unblock personalization.
  */

  char ck_value[MCM_UIM_CK_MAX_V01];
  /**<   Control key value. This value is a sequence of ASCII characters.*/
}mcm_uim_depersonalization_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_uim_aggregates
    @{
  */
typedef struct {

  uint8_t verify_left;
  /**<   Number of the remaining attempts to verify the personalization.*/

  uint8_t unblock_left;
  /**<   Number of the remaining attempts to unblock the personalization.*/
}mcm_uim_perso_retries_left_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_uim_aggregates
    @{
  */
typedef struct {

  mcm_uim_subscription_t_v01 subscription;

  mcm_uim_app_state_t_v01 app_state;

  mcm_uim_perso_feature_t_v01 perso_feature;

  uint8_t perso_retries;

  uint8_t perso_unblock_retries;

  mcm_uim_pin_state_t_v01 pin1_state;

  uint8_t pin1_num_retries;

  uint8_t puk1_num_retries;

  mcm_uim_pin_state_t_v01 pin2_state;

  uint8_t pin2_num_retries;

  uint8_t puk2_num_retries;
}mcm_uim_app_info_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_uim_aggregates
    @{
  */
typedef struct {

  mcm_uim_app_info_t_v01 app_3gpp;

  mcm_uim_app_info_t_v01 app_3gpp2;

  mcm_uim_app_info_t_v01 app_isim;
}mcm_uim_card_app_info_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_uim_aggregates
    @{
  */
typedef struct {

  mcm_uim_card_state_t_v01 card_state;

  mcm_uim_card_t_t_v01 card_t;

  mcm_uim_card_app_info_t_v01 card_app_info;
}mcm_uim_card_info_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_uim_aggregates
    @{
  */
typedef struct {

  uint16_t file_id;

  char path_value[MCM_UIM_MAX_BINARY_PATHS_V01];
}mcm_uim_refresh_file_list_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_uim_aggregates
    @{
  */
typedef struct {

  /*  slot */
  mcm_uim_slot_id_t_v01 slot_id;

  /*  app type */
  mcm_uim_app_type_t_v01 app_t;

  /*  refresh mode */
  mcm_uim_refresh_mode_t_v01 refresh_mode;

  /*  number of refresh files */
  uint8_t num_files;

  /*  refresh file data */
  mcm_uim_refresh_file_list_t_v01 refresh_files[MCM_UIM_MAX_REFRESH_FILES_V01];
}mcm_uim_refresh_event_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Retrives the IMSI value stored on the specified application */
typedef struct {

  /* Mandatory */
  /*  application identification */
  mcm_uim_application_identification_info_t_v01 app_info;
}mcm_uim_get_imsi_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Retrives the IMSI value stored on the specified application */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
  /**<   Standard response type. Contains the following data members:
       mcm_result_t -- MCM_RESULT_SUCCESS or MCM_RESULT_FAILURE.
       mcm_error_t  -- Error code. Possible error code values are described
                          in the error codes section of each message
                          definition.
  */

  /* Optional */
  /*  IMSI data */
  uint8_t imsi_valid;  /**< Must be set to true if imsi is being passed */
  char imsi[MCM_UIM_IMSI_LEN_V01];
  /**<   IMSI data in ASCII characters.*/
}mcm_uim_get_imsi_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Retrives the ICCID stored on the card */
typedef struct {

  /* Mandatory */
  /*  Slot ID */
  mcm_uim_slot_id_t_v01 slot_id;
  /**<   Indicates the slot to be used. Valid values:
        - 1 -- Slot 1
        - 2 -- Slot 2
  */
}mcm_uim_get_iccid_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Retrives the ICCID stored on the card */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
  /**<   Standard response type. Contains the following data members:
       mcm_result_t -- MCM_RESULT_SUCCESS or MCM_RESULT_FAILURE.
       mcm_error_t  -- Error code. Possible error code values are described
                          in the error codes section of each message
                          definition.
  */

  /* Optional */
  /*  ICCID data */
  uint8_t iccid_valid;  /**< Must be set to true if iccid is being passed */
  char iccid[MCM_UIM_ICCID_LEN_V01];
  /**<   ICCID data in ASCII characters.*/
}mcm_uim_get_iccid_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Retrives the device phone number stored on the card */
typedef struct {

  /* Mandatory */
  /*  application identification */
  mcm_uim_application_identification_info_t_v01 app_info;
}mcm_uim_get_dpn_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Retrives the device phone number stored on the card */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
  /**<   Standard response type. Contains the following data members:
       mcm_result_t -- MCM_RESULT_SUCCESS or MCM_RESULT_FAILURE.
       mcm_error_t  -- Error code. Possible error code values are described
                          in the error codes section of each message
                          definition.
  */

  /* Optional */
  /*  Device phone number */
  uint8_t phone_num_valid;  /**< Must be set to true if phone_num is being passed */
  char phone_num[MCM_UIM_PHONE_NUM_MAX_V01];
  /**<   Parsed phone number in ASCII characters.*/
}mcm_uim_get_dpn_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Retrives the preferred operator list stored on the card */
typedef struct {

  /* Mandatory */
  /*  application identification */
  mcm_uim_application_identification_info_t_v01 app_info;
}mcm_uim_get_plmn_list_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Retrives the preferred operator list stored on the card */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
  /**<   Standard response type. Contains the following data members:
       mcm_result_t -- MCM_RESULT_SUCCESS or MCM_RESULT_FAILURE.
       mcm_error_t  -- Error code. Possible error code values are described
                          in the error codes section of each message
                          definition.
  */

  /* Optional */
  /*  Preferred Operator List */
  uint8_t plmn_list_valid;  /**< Must be set to true if plmn_list is being passed */
  mcm_uim_plmn_t_v01 plmn_list[MCM_UIM_NUM_PLMN_MAX_V01];
  /**<   Token ID matching ASYNC request to response.*/
}mcm_uim_get_plmn_list_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Reads data to a specific file on a specified application on card */
typedef struct {

  /* Mandatory */
  /*  application identification */
  mcm_uim_application_identification_info_t_v01 app_info;

  /* Mandatory */
  /*  File access information */
  mcm_uim_file_access_t_v01 file_access;
}mcm_uim_read_file_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Reads data to a specific file on a specified application on card */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
  /**<   Standard response type. Contains the following data members:
       mcm_result_t -- MCM_RESULT_SUCCESS or MCM_RESULT_FAILURE.
       mcm_error_t  -- Error code. Possible error code values are described
                          in the error codes section of each message
                          definition.
  */

  /* Optional */
  /*  Card Result */
  uint8_t card_result_valid;  /**< Must be set to true if card_result is being passed */
  mcm_uim_card_result_t_v01 card_result;

  /* Optional */
  /*  Data retrieved from the card */
  uint8_t data_valid;  /**< Must be set to true if data is being passed */
  uint32_t data_len;  /**< Must be set to # of elements in data */
  uint8_t data[MCM_UIM_DATA_MAX_V01];
}mcm_uim_read_file_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Writes data to a specific file on a specified application on card */
typedef struct {

  /* Mandatory */
  /*  application identification */
  mcm_uim_application_identification_info_t_v01 app_info;

  /* Mandatory */
  /*  File access information */
  mcm_uim_file_access_t_v01 file_access;

  /* Mandatory */
  /*  Data to be updated on card */
  uint8_t data[MCM_UIM_DATA_MAX_V01];
}mcm_uim_write_file_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Writes data to a specific file on a specified application on card */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
  /**<   Standard response type. Contains the following data members:
       mcm_result_t -- MCM_RESULT_SUCCESS or MCM_RESULT_FAILURE.
       mcm_error_t  -- Error code. Possible error code values are described
                          in the error codes section of each message
                          definition.
  */

  /* Optional */
  /*  Card Result */
  uint8_t card_result_valid;  /**< Must be set to true if card_result is being passed */
  mcm_uim_card_result_t_v01 card_result;
}mcm_uim_write_file_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Retrieves the size of a specific file on a specified application on card */
typedef struct {

  /* Mandatory */
  /*  application identification */
  mcm_uim_application_identification_info_t_v01 app_info;

  /* Mandatory */
  /*  File path */
  char path[MCM_UIM_CHAR_PATH_MAX_V01];
  /**<   File path in ASCII characters.*/
}mcm_uim_get_file_size_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Retrieves the size of a specific file on a specified application on card */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
  /**<   Standard response type. Contains the following data members:
       mcm_result_t -- MCM_RESULT_SUCCESS or MCM_RESULT_FAILURE.
       mcm_error_t  -- Error code. Possible error code values are described
                          in the error codes section of each message
                          definition.
  */

  /* Optional */
  /*  Card Result */
  uint8_t card_result_valid;  /**< Must be set to true if card_result is being passed */
  mcm_uim_card_result_t_v01 card_result;

  /* Optional */
  /*  File information */
  uint8_t file_info_valid;  /**< Must be set to true if file_info is being passed */
  mcm_uim_file_info_t_v01 file_info;
}mcm_uim_get_file_size_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Verify the PIN value of an application */
typedef struct {

  /* Mandatory */
  /*  application identification */
  mcm_uim_application_identification_info_t_v01 app_info;

  /* Mandatory */
  /*  pin id */
  mcm_uim_pin_operation_t_v01 pin_id;

  /* Mandatory */
  /*  value of the pin */
  char pin_value[MCM_UIM_PIN_MAX_V01];
}mcm_uim_verify_pin_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Verify the PIN value of an application */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
  /**<   Standard response type. Contains the following data members:
       mcm_result_t -- MCM_RESULT_SUCCESS or MCM_RESULT_FAILURE.
       mcm_error_t  -- Error code. Possible error code values are described
                          in the error codes section of each message
                          definition.
  */

  /* Optional */
  /*  Retries Remaining */
  uint8_t retries_left_valid;  /**< Must be set to true if retries_left is being passed */
  uint8_t retries_left;
}mcm_uim_verify_pin_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Changes the PIN value of an application */
typedef struct {

  /* Mandatory */
  /*  application identification */
  mcm_uim_application_identification_info_t_v01 app_info;

  /* Mandatory */
  /*  pin id */
  mcm_uim_pin_operation_t_v01 pin_id;

  /* Mandatory */
  /*  Value of the old PIN in sequence of ASCII characters */
  char old_pin_value[MCM_UIM_PIN_MAX_V01];

  /* Mandatory */
  /*  Value of the new PIN in sequence of ASCII characters */
  char newpin_value[MCM_UIM_PIN_MAX_V01];
}mcm_uim_change_pin_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Changes the PIN value of an application */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
  /**<   Standard response type. Contains the following data members:
       mcm_result_t -- MCM_RESULT_SUCCESS or MCM_RESULT_FAILURE.
       mcm_error_t  -- Error code. Possible error code values are described
                          in the error codes section of each message
                          definition.
  */

  /* Optional */
  /*  Retries Remaining */
  uint8_t retries_left_valid;  /**< Must be set to true if retries_left is being passed */
  uint8_t retries_left;
}mcm_uim_change_pin_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Unblocks a blocked PIN using the PUK code. */
typedef struct {

  /* Mandatory */
  /*  application identification */
  mcm_uim_application_identification_info_t_v01 app_info;

  /* Mandatory */
  /*  pin id */
  mcm_uim_pin_operation_t_v01 pin_id;

  /* Mandatory */
  /*  Value of the PUK in sequence of ASCII characters */
  char puk_value[MCM_UIM_PIN_MAX_V01];

  /* Mandatory */
  /*  Value of the new PIN in sequence of ASCII characters */
  char new_pin_value[MCM_UIM_PIN_MAX_V01];
}mcm_uim_unblock_pin_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Unblocks a blocked PIN using the PUK code. */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
  /**<   Standard response type. Contains the following data members:
       mcm_result_t -- MCM_RESULT_SUCCESS or MCM_RESULT_FAILURE.
       mcm_error_t  -- Error code. Possible error code values are described
                          in the error codes section of each message
                          definition.
  */

  /* Optional */
  /*  Retries Remaining */
  uint8_t retries_left_valid;  /**< Must be set to true if retries_left is being passed */
  uint8_t retries_left;
}mcm_uim_unblock_pin_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Unblocks the PIN of an application */
typedef struct {

  /* Mandatory */
  /*  application identification */
  mcm_uim_application_identification_info_t_v01 app_info;

  /* Mandatory */
  /*  pin id */
  mcm_uim_pin_operation_t_v01 pin_id;

  /* Mandatory */
  /*  pin operation */
  mcm_uim_pin_operation_t_v01 pin_operation;

  /* Mandatory */
  /*  Value of the new PIN in sequence of ASCII characters */
  char pin_value[MCM_UIM_PIN_MAX_V01];
}mcm_uim_set_pin_status_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Unblocks the PIN of an application */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
  /**<   Standard response type. Contains the following data members:
       mcm_result_t -- MCM_RESULT_SUCCESS or MCM_RESULT_FAILURE.
       mcm_error_t  -- Error code. Possible error code values are described
                          in the error codes section of each message
                          definition.
  */

  /* Optional */
  /*  Retries Remaining */
  uint8_t retries_left_valid;  /**< Must be set to true if retries_left is being passed */
  uint8_t retries_left;
}mcm_uim_set_pin_status_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Deactivates or unblocks the personalization on the phone. */
typedef struct {

  /* Mandatory */
  /*  Depersonalization */
  mcm_uim_depersonalization_t_v01 depersonalization;
}mcm_uim_depersonalization_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Deactivates or unblocks the personalization on the phone. */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
  /**<   Standard response type. Contains the following data members:
       mcm_result_t -- MCM_RESULT_SUCCESS or MCM_RESULT_FAILURE.
       mcm_error_t  -- Error code. Possible error code values are described
                          in the error codes section of each message
                          definition.
  */

  /* Optional */
  /*  Retries Remaining */
  uint8_t retries_left_valid;  /**< Must be set to true if retries_left is being passed */
  mcm_uim_perso_retries_left_t_v01 retries_left;
}mcm_uim_depersonalization_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Retrives the card status stored of a card */
typedef struct {

  /* Mandatory */
  /*  slot */
  mcm_uim_slot_id_t_v01 slot_id;
}mcm_uim_get_card_status_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Retrives the card status stored of a card */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
  /**<   Standard response type. Contains the following data members:
       mcm_result_t -- MCM_RESULT_SUCCESS or MCM_RESULT_FAILURE.
       mcm_error_t  -- Error code. Possible error code values are described
                          in the error codes section of each message
                          definition.
  */

  /* Optional */
  /*  card information */
  uint8_t card_info_valid;  /**< Must be set to true if card_info is being passed */
  mcm_uim_card_info_t_v01 card_info;
}mcm_uim_get_card_status_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Request Message; Register for Indication of Events */
typedef struct {

  /* Optional */
  uint8_t register_card_status_event_valid;  /**< Must be set to true if register_card_status_event is being passed */
  uint8_t register_card_status_event;
  /**<   - MCM_SIM_CARD_STATUS_EV*/

  /* Optional */
  uint8_t register_refresh_event_valid;  /**< Must be set to true if register_refresh_event is being passed */
  uint8_t register_refresh_event;
  /**<   - MCM_SIM_REFRESH_EV*/
}mcm_sim_event_register_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Response Message; Register for Indication of Events */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_sim_event_register_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Indication Message; Indication corresponding to MCM_SIM_CARD_STATUS_EV */
typedef struct {

  /* Mandatory */
  /*  card information indication */
  uint32_t card_info_len;  /**< Must be set to # of elements in card_info */
  mcm_uim_card_info_t_v01 card_info[MCM_UIM_MAX_NUM_CARDS_V01];
}mcm_sim_card_status_event_ind_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_uim_messages
    @{
  */
/** Indication Message; Indication corresponding to MCM_SIM_REFRESH_EV */
typedef struct {

  /* Mandatory */
  /*  slot */
  mcm_uim_slot_id_t_v01 slot_id;

  /* Mandatory */
  /*  app type */
  mcm_uim_app_type_t_v01 app_t;

  /* Mandatory */
  /*  refresh mode */
  mcm_uim_refresh_mode_t_v01 refresh_mode;

  /* Mandatory */
  /*  number of refresh files */
  uint8_t num_files;

  /* Mandatory */
  /*  refresh file data */
  mcm_uim_refresh_file_list_t_v01 refresh_files[MCM_UIM_MAX_REFRESH_FILES_V01];
}mcm_sim_refresh_event_ind_msg_v01;  /* Message */
/**
    @}
  */

/*Service Message Definition*/
/** @addtogroup mcm_uim_msg_ids
    @{
  */
#define MCM_UIM_GET_SUBSCRIBER_ID_REQ_V01 0x0900
#define MCM_UIM_GET_SUBSCRIBER_ID_RESP_V01 0x0900
#define MCM_UIM_GET_CARD_ID_REQ_V01 0x0901
#define MCM_UIM_GET_CARD_ID_RESP_V01 0x0901
#define MCM_UIM_GET_DEVICE_PHONE_NUMBER_REQ_V01 0x0902
#define MCM_UIM_GET_DEVICE_PHONE_NUMBER_RESP_V01 0x0902
#define MCM_UIM_GET_PREFERRED_OPERATOR_LIST_REQ_V01 0x0903
#define MCM_UIM_GET_PREFERRED_OPERATOR_LIST_RESP_V01 0x0903
#define MCM_UIM_READ_FILE_REQ_V01 0x0904
#define MCM_UIM_READ_FILE_RESP_V01 0x0904
#define MCM_UIM_WRITE_FILE_REQ_V01 0x0905
#define MCM_UIM_WRITE_FILE_RESP_V01 0x0905
#define MCM_UIM_GET_FILE_SIZE_REQ_V01 0x0906
#define MCM_UIM_GET_FILE_SIZE_RESP_V01 0x0906
#define MCM_UIM_VERIFY_PIN_REQ_V01 0x0907
#define MCM_UIM_VERIFY_PIN_RESP_V01 0x0907
#define MCM_UIM_CHANGE_PIN_REQ_V01 0x0908
#define MCM_UIM_CHANGE_PIN_RESP_V01 0x0908
#define MCM_UIM_UNBLOCK_PIN_REQ_V01 0x0909
#define MCM_UIM_UNBLOCK_PIN_RESP_V01 0x0909
#define MCM_UIM_SET_PIN_STATUS_REQ_V01 0x090A
#define MCM_UIM_SET_PIN_STATUS_RESP_V01 0x090A
#define MCM_UIM_GET_CARD_STATUS_REQ_V01 0x090B
#define MCM_UIM_GET_CARD_STATUS_RESP_V01 0x090B
#define MCM_UIM_DEPERSONALIZATION_REQ_V01 0x090B
#define MCM_UIM_DEPERSONALIZATION_RESP_V01 0x090B
#define MCM_SIM_EVENT_REGISTER_REQ_V01 0x090C
#define MCM_SIM_EVENT_REGISTER_RESP_V01 0x090C
#define MCM_SIM_CARD_STATUS_EVENT_IND_V01 0x090D
#define MCM_SIM_REFRESH_EVENT_IND_V01 0x090E
/**
    @}
  */
#ifdef __cplusplus
}
#endif
#endif

