#ifndef MCM_NW_SERVICE_01_H
#define MCM_NW_SERVICE_01_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
#include <stdint.h>
#include "mcm_common_v01.h"


#ifdef __cplusplus
extern "C" {
#endif


/** @addtogroup mcm_nw_consts
    @{
  */
#define MCM_NW_NETWORK_REGISTRATION_EV_V01 0x3003
#define MCM_NW_VOICE_REGISTRATION_EV_V01 0x3004
#define MCM_NW_DATA_REGISTRATION_EV_V01 0x3005
#define MCM_NW_IMS_REGISTRATION_EV_V01 0x3006
#define MCM_NW_TIME_EV_V01 0x3010
#define MCM_NW_SIGNAL_STRENGTH_EV_V01 0x3011
#define MCM_NW_ERROR_RATE_EV_V01 0x3012
/**
    @}
  */

typedef uint64_t mcm_mode_type_v01;
#define MCM_MODE_NONE_V01 ((mcm_mode_type_v01)0x00ull) /**<  No network. */
#define MCM_MODE_GSM_V01 ((mcm_mode_type_v01)0x01ull) /**<  Include GSM networks. */
#define MCM_MODE_WCDMA_V01 ((mcm_mode_type_v01)0x02ull) /**<  Include WCDMA networks. */
#define MCM_MODE_CDMA_V01 ((mcm_mode_type_v01)0x04ull) /**<  Include CDMA networks. */
#define MCM_MODE_EVDO_V01 ((mcm_mode_type_v01)0x08ull) /**<  Include EVDO networks. */
#define MCM_MODE_LTE_V01 ((mcm_mode_type_v01)0x10ull) /**<  Include LTE networks. */
#define MCM_MODE_TDSCDMA_V01 ((mcm_mode_type_v01)0x20ull) /**<  Include TDSCDMA networks. */
#define MCM_MODE_3GPP2_V01 ((mcm_mode_type_v01)0x4000ull) /**<  3GPP2 preferred over 3GPP, default 3GPP. */
#define MCM_MODE_CS_V01 ((mcm_mode_type_v01)0x8000ull) /**<  CS prefered over PS, default is PS. */
#define MCM_MODE_PRL_V01 ((mcm_mode_type_v01)0x10000ull) /**<  Give preference according to Preferred
 Roaming List. */
/** @addtogroup mcm_nw_enums
    @{
  */
typedef enum {
  MCM_NW_SERVICE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_NW_SERVICE_NONE_V01 = 0x0000, /**<  Not registered or no data. */
  MCM_NW_SERVICE_LIMITED_V01 = 0x0001, /**<  Registered, emergancy service only. */
  MCM_NW_SERVICE_FULL_V01 = 0x0002, /**<  Registered, full service. */
  MCM_NW_SERVICE_DENIED_V01 = 0x0003, /**<  Registration denied, check
   mcm_nw_3gpp_registration for deny_reason. */
  MCM_NW_SERVICE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_nw_service_t_v01;
/**
    @}
  */

/** @addtogroup mcm_nw_enums
    @{
  */
typedef enum {
  MCM_NW_RADIO_TECH_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_NW_RADIO_TECH_TD_SCDMA_V01 = 1,
  MCM_NW_RADIO_TECH_GSM_V01 = 2, /**<  Only supports voice */
  MCM_NW_RADIO_TECH_HSPAP_V01 = 3, /**<  HSPA+ */
  MCM_NW_RADIO_TECH_LTE_V01 = 4,
  MCM_NW_RADIO_TECH_EHRPD_V01 = 5,
  MCM_NW_RADIO_TECH_EVDO_B_V01 = 6,
  MCM_NW_RADIO_TECH_HSPA_V01 = 7,
  MCM_NW_RADIO_TECH_HSUPA_V01 = 8,
  MCM_NW_RADIO_TECH_HSDPA_V01 = 9,
  MCM_NW_RADIO_TECH_EVDO_A_V01 = 10,
  MCM_NW_RADIO_TECH_EVDO_0_V01 = 11,
  MCM_NW_RADIO_TECH_1xRTT_V01 = 12,
  MCM_NW_RADIO_TECH_IS95B_V01 = 13,
  MCM_NW_RADIO_TECH_IS95A_V01 = 14,
  MCM_NW_RADIO_TECH_UMTS_V01 = 15,
  MCM_NW_RADIO_TECH_EDGE_V01 = 16,
  MCM_NW_RADIO_TECH_GPRS_V01 = 17,
  MCM_NW_RADIO_TECH_NONE_V01 = 18,
  MCM_NW_RADIO_TECH_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_nw_radio_tech_t_v01;
/**
    @}
  */

/** @addtogroup mcm_nw_enums
    @{
  */
typedef enum {
  MCM_NW_ROAM_STATE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_NW_ROAM_STATE_OFF_V01 = 12, /**<  None or roaming indicator off */
  MCM_NW_ROAM_STATE_ON_V01 = 13, /**<  romaing indicator on */
  MCM_NW_ROAM_STATE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_nw_roam_state_t_v01;
/**
    @}
  */

/** @addtogroup mcm_nw_aggregates
    @{
  */
typedef struct {

  uint16_t gsm_rssi;
  /**<   RSSI delta in units of 0.1 dBm, 0 default or ignore*/

  uint16_t reserved;
  /**<   word alignment*/

  uint16_t wcdma_rssi;
  /**<   RSSI delta in units of 0.1 dBm, 0 default or ignore*/

  uint16_t wcdma_ecio;
  /**<   ec/io delta in units of 0.1 dBm, 0 default or ignore*/

  uint16_t cdma_rssi;
  /**<   RSSI delta in units of 0.1 dBm, 0 default or ignore*/

  uint16_t cdma_ecio;
  /**<   ec/io delta in units of 0.1 dBm, 0 default or ignore*/

  uint16_t hdr_rssi;
  /**<   RSSI delta in units of 0.1 dBm, 0 default or ignore*/

  uint16_t hdr_ecio;
  /**<   ec/io delta in units of 0.1 dBm, 0 default or ignore*/

  uint16_t hdr_sinr;
  /**<   SINR delta in units of 1 SINR level, 0 default/ignore*/

  uint16_t hdr_io;
  /**<   IO delta in units of 0.1 dBm, 0 default/ignore*/

  uint16_t lte_rssi;
  /**<   RSSI delta in units of 0.1 dBm, 0 default or ignore*/

  uint16_t lte_snr;
  /**<   SNR delta in units of 0.1 dBm, 0 default or ignore*/

  uint16_t lte_rsrq;
  /**<   RSRQ delta in units of 0.1 dBm, 0 default or ignore*/

  uint16_t lte_rsrp;
  /**<   RSRP delta in units of 0.1 dBm, 0 default or ignore*/

  uint16_t lte_rate;
  /**<   rate at which to check the LTE signal for reporting,
 0 - default, 1-5 (sec).*/

  uint16_t lte_period;
  /**<   averaging period to be used for LTE signal,
 0 - default, 1-10 (sec).*/

  uint16_t tdscdma_rssi;
  /**<   RSSI delta in units of 0.1 dBm, 0 default or ignore*/

  uint16_t tdscdma_ecio;
  /**<   ec/io delta in units of 0.1 dBm, 0 default/ignore*/

  uint16_t tdscdma_sinr;
  /**<   SINR delta in units of 1 SINR level,
 0 default or ignore*/

  uint16_t tdscdma_rscp;
  /**<   RSCP delta in units of 0.1 dBm, 0 deafult or ignore*/
}mcm_nw_config_delta_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_nw_aggregates
    @{
  */
typedef struct {

  int8_t gsm_rssi;
  /**<   RSSI in dBm. Indicates received signal strength.
 a signed value, -125 or lower indicates no signal*/

  int8_t wcdma_rssi;
  /**<   RSSI in dBm. Indicates forward link pilot Ec.
 a signed value, -125 or lower indicates no signal*/

  int8_t cdma_rssi;
  /**<   RSSI in dBm. Indicates forward link pilot power
 (AGC) + Ec/Io
 a signed value, -125 or lower indicates no signal*/

  int8_t hdr_rssi;
  /**<   RSSI in dBm. Indicates forward link pilot power
 (AGC) + Ec/Io
 a signed value, -125 or lower indicates no signal*/

  int8_t lte_rssi;
  /**<   RSSI in dBm. Indicates forward link pilot Ec
 a signed value, -125 or lower indicates no signal*/

  float tdscdma_rssi;
  /**<   RSSI in dBm*/
}mcm_nw_signal_strength_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_nw_aggregates
    @{
  */
typedef struct {

  uint32_t tech;
  /**<   Technology, used to determine structure type
  mcm_tech, 0 - None, 1 - 3gpp, 2 - 3gpp2*/

  mcm_nw_radio_tech_t_v01 radio_tech;
  /**<   Radio technology @ref mcm_nw_radiotech*/

  uint32_t domain;
  /**<   Service domain registered @ref mcm_domain*/

  char mcc[3 + 1];
  /**<   Mobile Country Code*/

  char mnc[3 + 1];
  /**<   Mobile Network Code*/

  mcm_nw_roam_state_t_v01 roaming;
  /**<   0 - off, 1 - roaming (3gpp2 has extended values)*/

  uint8_t forbidden;
  /**<   0 - no, 1 - yes*/

  uint32_t deny_reason;
  /**<   Set when registration state is
  MCM_NW_SERVICE_DENIED,
 see 3GPP TS 24.00 10.5.3.6 and Annex G.*/
}mcm_nw_any_registration_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_nw_aggregates
    @{
  */
typedef struct {

  uint32_t tech;
  /**<   Technology, used to determine structure type
  mcm_tech, 0 - None, 1 - 3gpp, 2 - 3gpp2*/

  mcm_nw_radio_tech_t_v01 radio_tech;
  /**<   Radio technology  mcm_nw_radiotech*/

  uint32_t domain;
  /**<   Service domain registered  mcm_domain*/

  char mcc[3 + 1];
  /**<   Mobile Country Code*/

  char mnc[3 + 1];
  /**<   Mobile Network Code*/

  mcm_nw_roam_state_t_v01 roaming;
  /**<   0 - off, 1 - roaming (3gpp2 has extended values)*/

  uint8_t forbidden;
  /**<   0 - no, 1 - yes*/

  uint32_t cid;
  /**<   CID, cell id for the registered 3gpp system*/

  uint16_t lac;
  /**<   LAC, locatin area code for the registered 3gpp system*/

  uint16_t psc;
  /**<   Primary scrambling code (WCDMA only), 0 - None*/

  uint16_t tac;
  /**<   Tracking area code information for LTE.*/
}mcm_nw_3gpp_registration_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_nw_aggregates
    @{
  */
typedef struct {

  uint32_t tech;
  /**<   Technology, used to determine structure type
  mcm_tech, 0 - None, 1 - 3gpp, 2 - 3gpp2*/

  mcm_nw_radio_tech_t_v01 radio_tech;
  /**<   Radio technology  mcm_nw_radiotech*/

  uint32_t domain;
  /**<   Service domain registered  mcm_domain*/

  char mcc[3 + 1];
  /**<   Mobile Country Code*/

  char mnc[3 + 1];
  /**<   Mobile Network Code*/

  mcm_nw_roam_state_t_v01 roaming;
  /**<   Roaming status  mcm_nw_roam_state*/

  uint8_t forbidden;
  /**<   0 - no, 1 - yes*/

  uint8_t inPRL;
  /**<   0 not in PRL, 1 - in PRL*/

  uint8_t css;
  /**<   Concurrent services supported 0 - no, 1 - yes*/

  uint16_t sid;
  /**<   CDMA System id*/

  uint16_t nid;
  /**<   CDMA Network id*/

  uint16_t bsid;
  /**<   Base station id*/

  int32_t lattitude;
  /**<   Base station lattitude in units of 0.25 sec,
 expressed as 2's compliment signed. Positive is North*/

  int32_t longitude;
  /**<   Base station longitude in units of 0.25 sec
 expressed as 2's compliment signed. Positive is East*/
}mcm_nw_3gpp2_registration_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_nw_aggregates
    @{
  */
typedef struct {

  uint32_t tech;
  /**<   Technology, used to determine structure type
  mcm_tech, 0 - None, 1 - 3gpp, 2 - 3gpp2*/

  mcm_nw_any_registration_t_v01 reg_any;
  /**<   matches any tech, use to fetch basic data*/

  mcm_nw_3gpp_registration_t_v01 reg_3gpp;
  /**<   3gpp registrated system details*/

  mcm_nw_3gpp2_registration_t_v01 reg_3gpp2;
  /**<   3gpp3 registrated system details*/
}mcm_nw_reg_details_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_nw_aggregates
    @{
  */
typedef struct {

  uint64_t voice_mode;
  /**<   Preferred network mode for voice connections,
 a bitmask of  mcm_nw_mode*/

  uint64_t data_mode;
  /**<   Preferred network mode for data connections,
 a bitmask of  mcm_nw_mode*/

  mcm_nw_config_delta_t_v01 delta;
  /**<   delta at which to report various signal
 strength events*/
}mcm_nw_config_params_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_nw_aggregates
    @{
  */
typedef struct {

  uint32_t voice_registration;
  /**<   Voice registration status, one of  mcm_nw_registration_states*/

  uint32_t data_registration;
  /**<   Data registration status, one of  mcm_nw_registration_states*/

  mcm_nw_reg_details_t_v01 voice_details;
  /**<   Voice registerd system details*/

  mcm_nw_reg_details_t_v01 data_details;
  /**<   Data registerd system details*/

  mcm_nw_signal_strength_t_v01 signal_strength;
  /**<   last reported signal strength data*/
}mcm_nw_status_params_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_nw_messages
    @{
  */
/** Request Message; Configure the MCM nw interface. Configure the settings that define it. */
typedef struct {

  /* Mandatory */
  mcm_nw_config_params_t_v01 config;
}mcm_nw_config_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_nw_messages
    @{
  */
/** Response Message; Configure the MCM nw interface. Configure the settings that define it. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_nw_config_resp_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_nw_get_config_req_msg_v01;

/** @addtogroup mcm_nw_messages
    @{
  */
/** Response Message; Get the configuration status for this nw interface */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t config_valid;  /**< Must be set to true if config is being passed */
  mcm_nw_config_params_t_v01 config;
}mcm_nw_get_config_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_nw_messages
    @{
  */
/** Request Message; Get the status associated with connection of \<id\> */
typedef struct {

  /* Mandatory */
  uint32_t connection_id;
}mcm_nw_get_status_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_nw_messages
    @{
  */
/** Response Message; Get the status associated with connection of \<id\> */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  mcm_nw_status_params_t_v01 status;
}mcm_nw_get_status_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_nw_messages
    @{
  */
/** Request Message; Register for Indication of Events */
typedef struct {

  /* Optional */
  uint8_t register_network_registration_event_valid;  /**< Must be set to true if register_network_registration_event is being passed */
  uint8_t register_network_registration_event;

  /* Optional */
  uint8_t register_voice_registration_event_valid;  /**< Must be set to true if register_voice_registration_event is being passed */
  uint8_t register_voice_registration_event;

  /* Optional */
  uint8_t register_data_registration_event_valid;  /**< Must be set to true if register_data_registration_event is being passed */
  uint8_t register_data_registration_event;

  /* Optional */
  uint8_t register_ims_registration_event_valid;  /**< Must be set to true if register_ims_registration_event is being passed */
  uint8_t register_ims_registration_event;

  /* Optional */
  uint8_t register_time_event_valid;  /**< Must be set to true if register_time_event is being passed */
  uint8_t register_time_event;

  /* Optional */
  uint8_t register_signal_strength_event_valid;  /**< Must be set to true if register_signal_strength_event is being passed */
  uint8_t register_signal_strength_event;

  /* Optional */
  uint8_t register_error_rate_event_valid;  /**< Must be set to true if register_error_rate_event is being passed */
  uint8_t register_error_rate_event;
}mcm_nw_event_register_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_nw_messages
    @{
  */
/** Response Message; Register for Indication of Events */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_nw_event_register_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_nw_messages
    @{
  */
/** Indication Message; Indication for the registered Events */
typedef struct {

  /* Mandatory */
  int32_t event_id;
  /**<   Event ID thats gets populated from one of these below
        0x3003L - MCM_NW_NETWORK_REGISTRATION_EV
        0x3004L - MCM_NW_VOICE_REGISTRATION_EV
        0x3005L - MCM_NW_DATA_REGISTRATION_EV
        0x3006L - MCM_NW_IMS_REGISTRATION_EV
        0x3010L - MCM_NW_TIME_EV
        0x3011L - MCM_NW_SIGNAL_STRENGTH_EV
        0x3012L - MCM_NW_ERROR_RATE_EV
  */

  /* Optional */
  uint8_t signal_strength_valid;  /**< Must be set to true if signal_strength is being passed */
  mcm_nw_signal_strength_t_v01 signal_strength;
  /**<   signal strength data for MCM_NW_SIGNAL_STRENGTH_EV*/
}mcm_nw_unsol_event_ind_msg_v01;  /* Message */
/**
    @}
  */

/*Service Message Definition*/
/** @addtogroup mcm_nw_msg_ids
    @{
  */
#define MCM_NW_CONFIG_REQ_V01 0x0500
#define MCM_NW_CONFIG_RESP_V01 0x0500
#define MCM_NW_GET_CONFIG_REQ_V01 0x0501
#define MCM_NW_GET_CONFIG_RESP_V01 0x0501
#define MCM_NW_GET_STATUS_REQ_V01 0x0502
#define MCM_NW_GET_STATUS_RESP_V01 0x0502
#define MCM_NW_EVENT_REGISTER_REQ_V01 0x0503
#define MCM_NW_EVENT_REGISTER_RESP_V01 0x0503
#define MCM_NW_UNSOL_EVENT_IND_V01 0x0504
/**
    @}
  */


#ifdef __cplusplus
}
#endif
#endif

