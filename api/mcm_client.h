#ifndef MCM_CLIENT_H
#define MCM_CLIENT_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
  Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

#include "mcm_common_v01.h"
#include "mcm_types.h"

#ifdef __cplusplus
extern "C" {
#endif

//=============================================================================
// DATA TYPES
//=============================================================================

typedef uint32 mcm_client_handle_type;

typedef void (*mcm_client_async_cb)
(
  mcm_client_handle_type hndl,
  uint32                 msg_id,
  void                  *resp_c_struct,
  uint32                 resp_len,
  int32                  token_id
);

typedef void (*mcm_client_ind_cb)
(
  mcm_client_handle_type hndl,
  uint32                 msg_id,
  void                  *ind_c_struct,
  uint32                 ind_len
);

//=============================================================================
// FUNCTIONS
//=============================================================================

//=============================================================================
// FUNCTION: mcm_client_init <ind_cb> <default_resp_cb> <out: hndl>
//
// DESCRIPTION:
// This function initializes the MCM client library.
//
// This function should be called prior to any other APIs are called.
//
// @ingroup mcm_client_api
// @param[out] hndl Client handle associated with this instance.
// @param[in]  ind_cb: Indication callback.
// @param[in]  default_resp_cb: Default response callback for async methods.
//
// @return
//    MCM_SUCCESS -- 0 is success\n
//    MCM_ERROR_TBD
//=============================================================================
uint32 mcm_client_init
(
  mcm_client_handle_type   *hndl,
  mcm_client_ind_cb         ind_cb,
  mcm_client_async_cb       default_resp_cb
);

//=============================================================================
// FUNCTION: mcm_client_execute_command_async
//
// DESCRIPTION:
// This function sends a command asynchronously to the service.
//
// @ingroup mcm_client_api
//
// @return
//    MCM_SUCCESS -- 0 is success\n
//    MCM_ERROR_TBD...
//=============================================================================
uint32 mcm_client_execute_command_async
(
  mcm_client_handle_type     hndl,
  int                        msg_id,
  void                      *req_c_struct,
  int                        req_c_struct_len,
  void                      *resp_c_struct,
  int                        resp_c_struct_len,
  mcm_client_async_cb        async_resp_cb,
  void                      *token_id
);

#define MCM_CLIENT_EXECUTE_COMMAND_ASYNC(hndl, msg_id, req_ptr, resp_ptr, cb , token_id)  \
  mcm_client_execute_command_async(hndl, msg_id, req_ptr, sizeof(*(req_ptr)), resp_ptr, sizeof(*(resp_ptr)), cb, (token_id))

//=============================================================================
// FUNCTION: mcm_client_execute_command_sync
//
// DESCRIPTION:
// This function sends a command synchronously to the service.
//
// @ingroup mcm_client_api
//
// @return
//    MCM_SUCCESS -- 0 is success\n
//    MCM_ERROR_TBD...
//=============================================================================
uint32 mcm_client_execute_command_sync
(
  mcm_client_handle_type      hndl,
  int                         msg_id,
  void                       *req_c_struct,
  int                         req_c_struct_len,
  void                       *resp_c_struct,
  int                         resp_c_struct_len
);

#define MCM_CLIENT_EXECUTE_COMMAND_SYNC(hndl, msg_id, req_ptr, resp_ptr) \
  mcm_client_execute_command_sync(hndl, msg_id, req_ptr, sizeof(*(req_ptr)), resp_ptr, sizeof(*(resp_ptr)))

//=============================================================================
// FUNCTION: mcm_client_release <hndl>
//
// DESCRIPTION:
// Release the subscription to the client
// @ingroup mcm_client_api
//
// @param[in] hndl handle associated with this instance and event queue
//
// @return
//    MCM_SUCCESS -- 0 is success\n
//    MCM_ERROR_TBD...
//=============================================================================
uint32 mcm_client_release(mcm_client_handle_type hndl);

#ifdef __cplusplus
}
#endif

#endif // MCM_CLIENT_H
