#ifndef MCM_COMMON_SERVICE_01_H
#define MCM_COMMON_SERVICE_01_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif



/** @addtogroup mcm_common_consts
    @{
  */
#define MCM_MAX_PHONE_NUMBER_V01 82
/**
    @}
  */

/** @addtogroup mcm_common_enums
    @{
  */
typedef enum {
  MCM_CMD_STATE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_CMD_STATE_NONE_V01 = 0x0000, /**<  Command State - None */
  MCM_CMD_STATE_PENDING_V01 = 0x0001, /**<  Command State - Pending */
  MCM_CMD_STATE_SUCCESS_V01 = 0x0002, /**<  Command State - Success */
  MCM_CMD_STATE_ERROR_V01 = 0x0003, /**<  Command State - Error */
  MCM_CMD_STATE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_cmd_state_t_v01;
/**
    @}
  */

/** @addtogroup mcm_common_enums
    @{
  */
typedef enum {
  MCM_TECH_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_TECH_NONE_V01 = 0x0000, /**<  Network Technologies - Network technologies used to differentiate networks, determine
                                       capabilities, and to determine the call control protocols - None */
  MCM_TECH_3GPP_V01 = 0x0001, /**<  Network Technologies - Network technologies used to differentiate networks, determine
                                       capabilities, and to determine the call control protocols - 3GPP */
  MCM_TECH_3GPP2_V01 = 0x0002, /**<  Network Technologies - Network technologies used to differentiate networks, determine
                                       capabilities, and to determine the call control protocols - 3GPP2 */
  MCM_TECH_IMS_V01 = 0x0003, /**<  Network Technologies - Network technologies used to differentiate networks, determine
                                       capabilities, and to determine the call control protocols - IMS */
  MCM_TECH_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_tech_t_v01;
/**
    @}
  */

/** @addtogroup mcm_common_enums
    @{
  */
typedef enum {
  MCM_DOMAIN_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_DOMAIN_NONE_V01 = 0x0000, /**<  Network call Domains - Network call domains used to determine network capabilities and
                                     call preference - None  */
  MCM_DOMAIN_AUTO_V01 = 0x0001, /**<  Network call Domains - Network call domains used to determine network capabilities and
                                     call preference - Auto  */
  MCM_DOMAIN_BOTH_V01 = 0x0001, /**<  Network call Domains - Network call domains used to determine network capabilities and
                                     call preference - Both  */
  MCM_DOMAIN_CS_V01 = 0x0002, /**<  Network call Domains - Network call domains used to determine network capabilities and
                                     call preference - CS  */
  MCM_DOMAIN_PS_V01 = 0x0003, /**<  Network call Domains - Network call domains used to determine network capabilities and
                                     call preference - PS  */
  MCM_DOMAIN_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_domain_t_v01;
/**
    @}
  */

/** @addtogroup mcm_common_enums
    @{
  */
typedef enum {
  MCM_RESULT_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_RESULT_SUCCESS_V01 = 0,
  MCM_RESULT_FAILURE_V01 = 1,
  MCM_RESULT_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_result_t_v01;
/**
    @}
  */

/** @addtogroup mcm_common_enums
    @{
  */
typedef enum {
  MCM_ERROR_T_MIN_ENUM_VAL_V01 =-2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/,
  MCM_SUCCESS_V01 = 0, /**<  Success code */,
  MCM_ERROR_GENERIC_V01,
  MCM_ERROR_BADPARM_V01,
  MCM_ERROR_MEMORY_V01,
  MCM_ERROR_INVALID_STATE_V01,
  MCM_ERROR_MALFORMED_MSG_V01,
  MCM_ERROR_NO_MEMORY_V01,
  MCM_ERROR_INTERNAL_V01,
  MCM_ERROR_ABORTED_V01,
  MCM_ERROR_CLIENT_IDS_EXHAUSTED_V01,
  MCM_ERROR_UNABORTABLE_TRANSACTION_V01,
  MCM_ERROR_INVALID_CLIENT_ID_V01,
  MCM_ERROR_NO_THRESHOLDS_V01,
  MCM_ERROR_INVALID_HANDLE_V01,
  MCM_ERROR_INVALID_PROFILE_V01,
  MCM_ERROR_INVALID_PINID_V01,
  MCM_ERROR_INCORRECT_PIN_V01,
  MCM_ERROR_NO_NETWORK_FOUND_V01,
  MCM_ERROR_CALL_FAILED_V01,
  MCM_ERROR_OUT_OF_CALL_V01,
  MCM_ERROR_NOT_PROVISIONED_V01,
  MCM_ERROR_MISSING_ARG_V01,
  MCM_ERROR_ARG_TOO_LONG_V01,
  MCM_ERROR_INVALID_TX_ID_V01,
  MCM_ERROR_DEVICE_IN_USE_V01,
  MCM_ERROR_OP_NETWORK_UNSUPPORTED_V01,
  MCM_ERROR_OP_DEVICE_UNSUPPORTED_V01,
  MCM_ERROR_NO_EFFECT_V01,
  MCM_ERROR_NO_FREE_PROFILE_V01,
  MCM_ERROR_INVALID_PDP_TYPE_V01,
  MCM_ERROR_INVALID_TECH_PREF_V01,
  MCM_ERROR_INVALID_PROFILE_TYPE_V01,
  MCM_ERROR_INVALID_SERVICE_TYPE_V01,
  MCM_ERROR_INVALID_REGISTER_ACTION_V01,
  MCM_ERROR_INVALID_PS_ATTACH_ACTION_V01,
  MCM_ERROR_AUTHENTICATION_FAILED_V01,
  MCM_ERROR_PIN_BLOCKED_V01,
  MCM_ERROR_PIN_PERM_BLOCKED_V01,
  MCM_ERROR_SIM_NOT_INITIALIZED_V01,
  MCM_ERROR_MAX_QOS_REQUESTS_IN_USE_V01,
  MCM_ERROR_INCORRECT_FLOW_FILTER_V01,
  MCM_ERROR_NETWORK_QOS_UNAWARE_V01,
  MCM_ERROR_INVALID_ID_V01,
  MCM_ERROR_INVALID_QOS_ID_V01,
  MCM_ERROR_REQUESTED_NUM_UNSUPPORTED_V01,
  MCM_ERROR_INTERFACE_NOT_FOUND_V01,
  MCM_ERROR_FLOW_SUSPENDED_V01,
  MCM_ERROR_INVALID_DATA_FORMAT_V01,
  MCM_ERROR_GENERAL_V01,
  MCM_ERROR_UNKNOWN_V01,
  MCM_ERROR_INVALID_ARG_V01,
  MCM_ERROR_INVALID_INDEX_V01,
  MCM_ERROR_NO_ENTRY_V01,
  MCM_ERROR_DEVICE_STORAGE_FULL_V01,
  MCM_ERROR_DEVICE_NOT_READY_V01,
  MCM_ERROR_NETWORK_NOT_READY_V01,
  MCM_ERROR_CAUSE_CODE_V01,
  MCM_ERROR_MESSAGE_NOT_SENT_V01,
  MCM_ERROR_MESSAGE_DELIVERY_FAILURE_V01,
  MCM_ERROR_INVALID_MESSAGE_ID_V01,
  MCM_ERROR_ENCODING_V01,
  MCM_ERROR_AUTHENTICATION_LOCK_V01,
  MCM_ERROR_INVALID_TRANSITION_V01,
  MCM_ERROR_NOT_A_MCAST_IFACE_V01,
  MCM_ERROR_MAX_MCAST_REQUESTS_IN_USE_V01,
  MCM_ERROR_INVALID_MCAST_HANDLE_V01,
  MCM_ERROR_INVALID_IP_FAMILY_PREF_V01,
  MCM_ERROR_SESSION_INACTIVE_V01,
  MCM_ERROR_SESSION_INVALID_V01,
  MCM_ERROR_SESSION_OWNERSHIP_V01,
  MCM_ERROR_INSUFFICIENT_RESOURCES_V01,
  MCM_ERROR_DISABLED_V01,
  MCM_ERROR_INVALID_OPERATION_V01,
  MCM_ERROR_INVALID_CMD_V01,
  MCM_ERROR_TPDU_TYPE_V01,
  MCM_ERROR_SMSC_ADDR_V01,
  MCM_ERROR_INFO_UNAVAILABLE_V01,
  MCM_ERROR_SEGMENT_TOO_LONG_V01,
  MCM_ERROR_SEGMENT_ORDER_V01,
  MCM_ERROR_BUNDLING_NOT_SUPPORTED_V01,
  MCM_ERROR_OP_PARTIAL_FAILURE_V01,
  MCM_ERROR_POLICY_MISMATCH_V01,
  MCM_ERROR_SIM_FILE_NOT_FOUND_V01,
  MCM_ERROR_EXTENDED_INTERNAL_V01,
  MCM_ERROR_ACCESS_DENIED_V01,
  MCM_ERROR_HARDWARE_RESTRICTED_V01,
  MCM_ERROR_ACK_NOT_SENT_V01,
  MCM_ERROR_INJECT_TIMEOUT_V01,
  MCM_ERROR_INCOMPATIBLE_STATE_V01,
  MCM_ERROR_FDN_RESTRICT_V01,
  MCM_ERROR_SUPS_FAILURE_CAUSE_V01,
  MCM_ERROR_NO_RADIO_V01,
  MCM_ERROR_NOT_SUPPORTED_V01,
  MCM_ERROR_NO_SUBSCRIPTION_V01,
  MCM_ERROR_CARD_CALL_CONTROL_FAILED_V01,
  MCM_ERROR_NETWORK_ABORTED_V01,
  MCM_ERROR_MSG_BLOCKED_V01,
  MCM_ERROR_INVALID_SESSION_TYPE_V01,
  MCM_ERROR_INVALID_PB_TYPE_V01,
  MCM_ERROR_NO_SIM_V01,
  MCM_ERROR_PB_NOT_READY_V01,
  MCM_ERROR_PIN_RESTRICTION_V01,
  MCM_ERROR_PIN2_RESTRICTION_V01,
  MCM_ERROR_PUK_RESTRICTION_V01,
  MCM_ERROR_PUK2_RESTRICTION_V01,
  MCM_ERROR_PB_ACCESS_RESTRICTED_V01,
  MCM_ERROR_PB_DELETE_IN_PROG_V01,
  MCM_ERROR_PB_TEXT_TOO_LONG_V01,
  MCM_ERROR_PB_NUMBER_TOO_LONG_V01,
  MCM_ERROR_PB_HIDDEN_KEY_RESTRICTION_V01,
  MCM_ERROR_PB_NOT_AVAILABLE_V01,
  MCM_ERROR_DEVICE_MEMORY_ERROR_V01,
  MCM_ERROR_SIM_PIN_BLOCKED_V01,
  MCM_ERROR_SIM_PIN_NOT_INITIALIZED_V01,
  MCM_ERROR_SIM_INVALID_PIN_V01,
  MCM_ERROR_SIM_INVALID_PERSO_CK_V01,
  MCM_ERROR_SIM_PERSO_BLOCKED_V01,
  MCM_ERROR_SIM_PERSO_INVALID_DATA_V01,
  MCM_ERROR_SIM_ACCESS_DENIED_V01,
  MCM_ERROR_SIM_INVALID_FILE_PATH_V01,
  MCM_ERROR_SIM_SERVICE_NOT_SUPPORTED_V01,
  MCM_ERROR_SIM_AUTH_FAIL_V01,
  MCM_ERROR_SIM_PIN_PERM_BLOCKED_V01,
  MCM_ERROR_T_MAX_ENUM_VAL_V01,
  MCM_ERROR_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_error_t_v01;
/**
    @}
  */

/** @addtogroup mcm_common_aggregates
    @{
  */
typedef struct {

  mcm_result_t_v01 result;
  /**<   Result code
                            - MCM_RESULT_SUCCESS
                            - MCM_RESULT_FAILURE
                          */

  mcm_error_t_v01 error;
  /**<   Error code. Possible error code values are
                               described in the error codes section of each
                               message definition.
                          */
}mcm_response_t_v01;  /* Type */
/**
    @}
  */

typedef mcm_error_t_v01 mcm_error_t;
typedef mcm_result_t_v01 mcm_result_t;
typedef mcm_response_t_v01 mcm_response_t;

#ifdef __cplusplus
}
#endif
#endif

