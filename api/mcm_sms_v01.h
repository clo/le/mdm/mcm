#ifndef MCM_SMS_SERVICE_01_H
#define MCM_SMS_SERVICE_01_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


#include <stdint.h>
#include "mcm_common_v01.h"


#ifdef __cplusplus
extern "C" {
#endif


/** @addtogroup mcm_sms_consts
    @{
  */
#define MCM_SMS_ADDRESS_DIGIT_MAX_V01 21
#define MCM_SMS_MESSAGE_LENGTH_MAX_V01 255
#define MCM_SMS_ETWS_MESSAGE_LENGTH_MAX_V01 1252
#define MCM_SMS_SMSC_ADDRESS_LENGTH_MAX_V01 11
#define MCM_SMS_ROUTE_TUPLE_MAX_V01 10
/**
    @}
  */

/** @addtogroup mcm_sms_enums
    @{
  */
typedef enum {
  MCM_SMS_MESSAGE_PROTOCOL_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_SMS_MESSAGE_PROTOCOL_WCDMA_V01 = 0x01,
  MCM_SMS_MESSAGE_PROTOCOL_CDMA_V01 = 0x02,
  MCM_SMS_MESSAGE_PROTOCOL_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_sms_message_protocol_enum_v01;
/**
    @}
  */

/** @addtogroup mcm_sms_enums
    @{
  */
typedef enum {
  MCM_SMS_MESSAGE_CLASS_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_SMS_MESSAGE_CLASS_NONE_V01 = 0x01,
  MCM_SMS_MESSAGE_CLASS_CDMA_V01 = 0x02,
  MCM_SMS_MESSAGE_CLASS_0_V01 = 0x03,
  MCM_SMS_MESSAGE_CLASS_1_V01 = 0x04,
  MCM_SMS_MESSAGE_CLASS_2_V01 = 0x05,
  MCM_SMS_MESSAGE_CLASS_3_V01 = 0x06,
  MCM_SMS_MESSAGE_CLASS_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_sms_message_class_enum_v01;
/**
    @}
  */

/** @addtogroup mcm_sms_enums
    @{
  */
typedef enum {
  MCM_SMS_RECEIPT_ACTION_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_SMS_UNKNOWN_V01 = 0x01,
  MCM_SMS_STORE_AND_NOTIFY_V01 = 0x02,
  MCM_SMS_DISCARD_V01 = 0x03,
  MCM_SMS_TRANSFER_AND_ACK_V01 = 0x04,
  MCM_SMS_TRANSFER_ONLY_V01 = 0x05,
  MCM_SMS_RECEIPT_ACTION_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_sms_receipt_action_enum_v01;
/**
    @}
  */

/** @addtogroup mcm_sms_enums
    @{
  */
typedef enum {
  MCM_SMS_MESSAGE_MODE_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_SMS_MESSAGE_MODE_GW_V01 = 0x01,
  MCM_SMS_MESSAGE_MODE_CDMA_V01 = 0x02,
  MCM_SMS_MESSAGE_MODE_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_sms_message_mode_enum_v01;
/**
    @}
  */

/** @addtogroup mcm_sms_enums
    @{
  */
typedef enum {
  MCM_SMS_MESSAGE_TYPE_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_SMS_MESSAGE_TYPE_POINT_TO_POINT_V01 = 0x01,
  MCM_SMS_MESSAGE_TYPE_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_sms_message_type_enum_v01;
/**
    @}
  */

/** @addtogroup mcm_sms_enums
    @{
  */
typedef enum {
  MCM_SMS_TRANSFER_IND_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_SMS_TRANSFER_IND_CLIENT_V01 = 0x01,
  MCM_SMS_TRANSFER_IND_SIM_V01 = 0x02,
  MCM_SMS_TRANSFER_IND_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_sms_transfer_ind_enum_v01;
/**
    @}
  */

/** @addtogroup mcm_sms_enums
    @{
  */
typedef enum {
  MCM_SMS_MESSAGE_FORMAT_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_SMS_MESSAGE_FORMAT_GW_PP_V01 = 0x01,
  MCM_SMS_MESSAGE_FORMAT_GW_BC_V01 = 0x02,
  MCM_SMS_MESSAGE_FORMAT_CDMA_V01 = 0x03,
  MCM_SMS_MESSAGE_FORMAT_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_sms_message_format_enum_v01;
/**
    @}
  */

/** @addtogroup mcm_sms_enums
    @{
  */
typedef enum {
  MCM_SMS_STORAGE_TYPE_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_SMS_STORAGE_TYPE_NONE_V01 = 0x01,
  MCM_SMS_STORAGE_TYPE_NV_V01 = 0x02,
  MCM_SMS_STORAGE_TYPE_UIM_V01 = 0x03,
  MCM_SMS_STORAGE_TYPE_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_sms_storage_type_enum_v01;
/**
    @}
  */

/** @addtogroup mcm_sms_enums
    @{
  */
typedef enum {
  MCM_SMS_MESSAGE_TAG_TYPE_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_SMS_TAG_TYPE_MO_NOT_SENT_V01 = 0x01,
  MCM_SMS_TAG_TYPE_MO_SENT_V01 = 0x02,
  MCM_SMS_TAG_TYPE_MT_NOT_READ_V01 = 0x03,
  MCM_SMS_TAG_TYPE_MT_READ_V01 = 0x04,
  MCM_SMS_MESSAGE_TAG_TYPE_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_sms_message_tag_type_enum_v01;
/**
    @}
  */

/** @addtogroup mcm_sms_aggregates
    @{
  */
typedef struct {

  mcm_sms_message_mode_enum_v01 message_mode;

  uint8_t bc_activate;
}mcm_sms_broadcast_activation_info_type_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_sms_enums
    @{
  */
typedef enum {
  MCM_SMS_TRANSPORT_NW_REG_STATUS_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_SMS_TRANSPORT_NW_REG_STATUS_FULL_SERVICE_V01 = 0x01,
  MCM_SMS_TRANSPORT_NW_REG_STATUS_LIMITED_SERVICE_V01 = 0x02,
  MCM_SMS_TRANSPORT_NW_REG_STATUS_IN_PROGRESS_V01 = 0x03,
  MCM_SMS_TRANSPORT_NW_REG_STATUS_NO_SERVICE_V01 = 0x04,
  MCM_SMS_TRANSPORT_NW_REG_STATUS_FAILED_V01 = 0x05,
  MCM_SMS_TRANSPORT_NW_REG_STATUS_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_sms_transport_nw_reg_status_enum_v01;
/**
    @}
  */

/** @addtogroup mcm_sms_enums
    @{
  */
typedef enum {
  MCM_SMS_SERVICE_READY_STATUS_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_SMS_SERVICE_READY_STATUS_3GPP_AND_3GPP2_V01 = 0x01,
  MCM_SMS_SERVICE_READY_STATUS_3GPP_V01 = 0x02,
  MCM_SMS_SERVICE_READY_STATUS_3GPP2_V01 = 0x03,
  MCM_SMS_SERVICE_READY_STATUS_NONE_V01 = 0x04,
  MCM_SMS_SERVICE_READY_STATUS_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_sms_service_ready_status_enum_v01;
/**
    @}
  */

/** @addtogroup mcm_sms_aggregates
    @{
  */
typedef struct {

  mcm_sms_storage_type_enum_v01 storage_type;

  uint32_t storage_index;
}mcm_sms_mt_message_type_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_sms_enums
    @{
  */
typedef enum {
  MCM_SMS_ACK_INDICATOR_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_SMS_ACK_INDICATOR_DO_NOT_SEND_ACK_V01 = 0x01,
  MCM_SMS_ACK_INDICATOR_SEND_ACK_V01 = 0x02,
  MCM_SMS_ACK_INDICATOR_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_sms_ack_indicator_enum_v01;
/**
    @}
  */

/** @addtogroup mcm_sms_enums
    @{
  */
typedef enum {
  MCM_SMS_ETWS_NOTIFICATION_TYPE_ENUM_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_SMS_ETWS_NOTIFICATION_TYPE_PRIMARY_V01 = 0x01,
  MCM_SMS_ETWS_NOTIFICATION_TYPE_SECONDARY_UMTS_V01 = 0x02,
  MCM_SMS_ETWS_NOTIFICATION_TYPE_SECONDARY_GSM_V01 = 0x03,
  MCM_SMS_ETWS_NOTIFICATION_TYPE_ENUM_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_sms_etws_notification_type_enum_v01;
/**
    @}
  */

/** @addtogroup mcm_sms_aggregates
    @{
  */
typedef struct {

  mcm_sms_etws_notification_type_enum_v01 notification_type;

  uint32_t data_len;  /**< Must be set to # of elements in data */
  uint8_t data[MCM_SMS_ETWS_MESSAGE_LENGTH_MAX_V01];
}mcm_sms_etws_message_type_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_sms_aggregates
    @{
  */
typedef struct {

  mcm_sms_ack_indicator_enum_v01 ack_indicator;

  uint32_t transaction_id;

  mcm_sms_message_format_enum_v01 format;

  uint32_t data_len;  /**< Must be set to # of elements in data */
  uint8_t data[MCM_SMS_MESSAGE_LENGTH_MAX_V01];
}mcm_sms_transfer_route_mt_message_type_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_sms_aggregates
    @{
  */
typedef struct {

  uint32_t data_len;  /**< Must be set to # of elements in data */
  uint8_t data[MCM_SMS_SMSC_ADDRESS_LENGTH_MAX_V01];
}mcm_sms_mt_message_smsc_address_type_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_sms_aggregates
    @{
  */
typedef struct {

  mcm_sms_mt_message_type_v01 mt_message;

  mcm_sms_transfer_route_mt_message_type_v01 transfer_route_mt_message;

  mcm_sms_message_mode_enum_v01 message_mode;

  mcm_sms_etws_message_type_v01 etws_message;

  mcm_sms_mt_message_smsc_address_type_v01 mt_message_smsc_address;

  uint8_t sms_on_ims;
}mcm_sms_incoming_sms_msg_type_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_sms_aggregates
    @{
  */
typedef struct {

  mcm_sms_message_type_enum_v01 message_type;

  mcm_sms_message_class_enum_v01 message_class;

  mcm_sms_storage_type_enum_v01 route_storage;

  mcm_sms_receipt_action_enum_v01 receipt_action;
}mcm_sms_set_route_list_tuple_type_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Request Message; Configure the MCM sms interface. Configure the settings that define it. */
typedef struct {

  /* Optional */
  uint8_t route_list_tuple_valid;  /**< Must be set to true if route_list_tuple is being passed */
  uint32_t route_list_tuple_len;  /**< Must be set to # of elements in route_list_tuple */
  mcm_sms_set_route_list_tuple_type_v01 route_list_tuple[MCM_SMS_ROUTE_TUPLE_MAX_V01];

  /* Optional */
  uint8_t transfer_ind_valid;  /**< Must be set to true if transfer_ind is being passed */
  mcm_sms_transfer_ind_enum_v01 transfer_ind;

  /* Optional */
  uint8_t smsc_address_digits_valid;  /**< Must be set to true if smsc_address_digits is being passed */
  char smsc_address_digits[MCM_SMS_ADDRESS_DIGIT_MAX_V01 + 1];

  /* Optional */
  uint8_t retry_period_valid;  /**< Must be set to true if retry_period is being passed */
  uint32_t retry_period;

  /* Optional */
  uint8_t retry_interval_valid;  /**< Must be set to true if retry_interval is being passed */
  uint32_t retry_interval;

  /* Optional */
  uint8_t mcm_sms_broadcast_activation_info_valid;  /**< Must be set to true if mcm_sms_broadcast_activation_info is being passed */
  mcm_sms_broadcast_activation_info_type_v01 mcm_sms_broadcast_activation_info;
}mcm_sms_set_config_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Response Message; Configure the MCM sms interface. Configure the settings that define it. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_sms_set_config_resp_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_sms_get_config_req_msg_v01;

/** @addtogroup mcm_sms_messages
    @{
  */
/** Response Message; Retrieves the configuration of the MCM sms interface. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t route_list_tuple_valid;  /**< Must be set to true if route_list_tuple is being passed */
  uint32_t route_list_tuple_len;  /**< Must be set to # of elements in route_list_tuple */
  mcm_sms_set_route_list_tuple_type_v01 route_list_tuple[MCM_SMS_ROUTE_TUPLE_MAX_V01];

  /* Optional */
  uint8_t transfer_ind_valid;  /**< Must be set to true if transfer_ind is being passed */
  mcm_sms_transfer_ind_enum_v01 transfer_ind;

  /* Optional */
  uint8_t smsc_address_digits_valid;  /**< Must be set to true if smsc_address_digits is being passed */
  char smsc_address_digits[MCM_SMS_ADDRESS_DIGIT_MAX_V01 + 1];

  /* Optional */
  uint8_t retry_period_valid;  /**< Must be set to true if retry_period is being passed */
  uint32_t retry_period;

  /* Optional */
  uint8_t retry_interval_valid;  /**< Must be set to true if retry_interval is being passed */
  uint32_t retry_interval;

  /* Optional */
  uint8_t mcm_sms_broadcast_activation_info_valid;  /**< Must be set to true if mcm_sms_broadcast_activation_info is being passed */
  mcm_sms_broadcast_activation_info_type_v01 mcm_sms_broadcast_activation_info;
}mcm_sms_get_config_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Request Message; Register for the events. */
typedef struct {

  /* Optional */
  uint8_t report_mt_message_valid;  /**< Must be set to true if report_mt_message is being passed */
  uint8_t report_mt_message;

  /* Optional */
  uint8_t reg_transport_layer_info_events_valid;  /**< Must be set to true if reg_transport_layer_info_events is being passed */
  uint8_t reg_transport_layer_info_events;

  /* Optional */
  uint8_t reg_service_ready_events_valid;  /**< Must be set to true if reg_service_ready_events is being passed */
  uint8_t reg_service_ready_events;
}mcm_sms_set_event_report_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Response Message; Register for the events. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_sms_set_event_report_resp_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_sms_get_event_report_req_msg_v01;

/** @addtogroup mcm_sms_messages
    @{
  */
/** Response Message; Retrieves the events that have been registered. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t report_mt_message_valid;  /**< Must be set to true if report_mt_message is being passed */
  uint8_t report_mt_message;

  /* Optional */
  uint8_t reg_transport_layer_info_events_valid;  /**< Must be set to true if reg_transport_layer_info_events is being passed */
  uint8_t reg_transport_layer_info_events;

  /* Optional */
  uint8_t reg_service_ready_events_valid;  /**< Must be set to true if reg_service_ready_events is being passed */
  uint8_t reg_service_ready_events;
}mcm_sms_get_event_report_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Request Message; Writes SMS. */
typedef struct {

  /* Mandatory */
  mcm_sms_storage_type_enum_v01 storage_type;

  /* Mandatory */
  uint32_t storage_index;

  /* Optional */
  uint8_t message_mode_valid;  /**< Must be set to true if message_mode is being passed */
  mcm_sms_message_mode_enum_v01 message_mode;

  /* Optional */
  uint8_t sms_on_ims_valid;  /**< Must be set to true if sms_on_ims is being passed */
  uint8_t sms_on_ims;
}mcm_sms_read_sms_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Response Message; Writes SMS. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Mandatory */
  mcm_sms_message_tag_type_enum_v01 tag_type;

  /* Mandatory */
  mcm_sms_message_format_enum_v01 format;

  /* Mandatory */
  uint32_t sms_payload_len;  /**< Must be set to # of elements in sms_payload */
  uint8_t sms_payload[MCM_SMS_MESSAGE_LENGTH_MAX_V01];
}mcm_sms_read_sms_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Request Message; Writes SMS. */
typedef struct {

  /* Mandatory */
  mcm_sms_storage_type_enum_v01 storage_type;

  /* Mandatory */
  mcm_sms_message_format_enum_v01 format;

  /* Mandatory */
  uint32_t sms_payload_len;  /**< Must be set to # of elements in sms_payload */
  uint8_t sms_payload[MCM_SMS_MESSAGE_LENGTH_MAX_V01];
}mcm_sms_write_sms_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Response Message; Writes SMS. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_sms_write_sms_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Request Message; Deletes SMS. */
typedef struct {

  /* Mandatory */
  mcm_sms_storage_type_enum_v01 storage_type;

  /* Optional */
  uint8_t index_valid;  /**< Must be set to true if index is being passed */
  uint32_t index;

  /* Optional */
  uint8_t tag_type_valid;  /**< Must be set to true if tag_type is being passed */
  mcm_sms_message_tag_type_enum_v01 tag_type;

  /* Optional */
  uint8_t message_mode_valid;  /**< Must be set to true if message_mode is being passed */
  mcm_sms_message_mode_enum_v01 message_mode;
}mcm_sms_delete_sms_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Response Message; Deletes SMS. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_sms_delete_sms_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Request Message; Sends SMS. */
typedef struct {

  /* Mandatory */
  mcm_sms_message_format_enum_v01 format;

  /* Mandatory */
  char sms_payload[MCM_SMS_MESSAGE_LENGTH_MAX_V01 + 1];

  /* Mandatory */
  char smsc_address[MCM_SMS_ADDRESS_DIGIT_MAX_V01 + 1];

  /* Optional */
  uint8_t sms_on_ims_valid;  /**< Must be set to true if sms_on_ims is being passed */
  uint8_t sms_on_ims;

  /* Optional */
  uint8_t retry_message_id_valid;  /**< Must be set to true if retry_message_id is being passed */
  uint32_t retry_message_id;
}mcm_sms_send_sms_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Response Message; Sends SMS. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_sms_send_sms_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Request Message; Acks SMS. */
typedef struct {

  /* Mandatory */
  uint32_t transaction_id;

  /* Mandatory */
  mcm_sms_message_protocol_enum_v01 message_protocol;

  /* Mandatory */
  uint8_t success;

  /* Optional */
  uint8_t error_class_valid;  /**< Must be set to true if error_class is being passed */
  uint32_t error_class;

  /* Optional */
  uint8_t tl_status_valid;  /**< Must be set to true if tl_status is being passed */
  uint32_t tl_status;

  /* Optional */
  uint8_t rp_cause_valid;  /**< Must be set to true if rp_cause is being passed */
  uint32_t rp_cause;

  /* Optional */
  uint8_t tp_cause_valid;  /**< Must be set to true if tp_cause is being passed */
  uint32_t tp_cause;
}mcm_sms_ack_sms_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Response Message; Acks SMS. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_sms_ack_sms_resp_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_sms_get_memory_status_req_msg_v01;

/** @addtogroup mcm_sms_messages
    @{
  */
/** Response Message; Gets memory status. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t memory_available_valid;  /**< Must be set to true if memory_available is being passed */
  uint8_t memory_available;
}mcm_sms_get_memory_status_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Request Message; Sets memory status. */
typedef struct {

  /* Mandatory */
  uint8_t memory_available;
}mcm_sms_set_memory_status_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Response Message; Sets memory status. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_sms_set_memory_status_resp_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_sms_get_transport_layer_info_req_msg_v01;

/** @addtogroup mcm_sms_messages
    @{
  */
/** Response Message; Retrieves the transport layer information. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t transport_nw_reg_status_valid;  /**< Must be set to true if transport_nw_reg_status is being passed */
  mcm_sms_transport_nw_reg_status_enum_v01 transport_nw_reg_status;
}mcm_sms_get_transport_layer_info_resp_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_sms_get_service_ready_status_req_msg_v01;

/** @addtogroup mcm_sms_messages
    @{
  */
/** Response Message; Retrieves the service ready status information. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t ready_status_valid;  /**< Must be set to true if ready_status is being passed */
  mcm_sms_service_ready_status_enum_v01 ready_status;
}mcm_sms_get_service_ready_status_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_sms_messages
    @{
  */
/** Indication Message; Indicates primary SMS information. */
typedef struct {

  /* Optional */
  uint8_t mcm_sms_incoming_sms_msg_valid;  /**< Must be set to true if mcm_sms_incoming_sms_msg is being passed */
  mcm_sms_incoming_sms_msg_type_v01 mcm_sms_incoming_sms_msg;

  /* Optional */
  uint8_t ready_status_valid;  /**< Must be set to true if ready_status is being passed */
  mcm_sms_service_ready_status_enum_v01 ready_status;

  /* Optional */
  uint8_t transport_nw_reg_status_valid;  /**< Must be set to true if transport_nw_reg_status is being passed */
  mcm_sms_transport_nw_reg_status_enum_v01 transport_nw_reg_status;
}mcm_sms_primary_information_ind_msg_v01;  /* Message */
/**
    @}
  */

/*Service Message Definition*/
/** @addtogroup mcm_sms_msg_ids
    @{
  */
#define MCM_SMS_SET_CONFIG_REQ_V01 0x0700
#define MCM_SMS_SET_CONFIG_RESP_V01 0x0700
#define MCM_SMS_GET_CONFIG_REQ_V01 0x0701
#define MCM_SMS_GET_CONFIG_RESP_V01 0x0701
#define MCM_SMS_SET_EVENT_REPORT_REQ_V01 0x0702
#define MCM_SMS_SET_EVENT_REPORT_RESP_V01 0x0702
#define MCM_SMS_GET_EVENT_REPORT_REQ_V01 0x0703
#define MCM_SMS_GET_EVENT_REPORT_RESP_V01 0x0703
#define MCM_SMS_READ_SMS_REQ_V01 0x0704
#define MCM_SMS_READ_SMS_RESP_V01 0x0704
#define MCM_SMS_WRITE_SMS_REQ_V01 0x0705
#define MCM_SMS_WRITE_SMS_RESP_V01 0x0705
#define MCM_SMS_DELETE_SMS_REQ_V01 0x0706
#define MCM_SMS_DELETE_SMS_RESP_V01 0x0706
#define MCM_SMS_SEND_SMS_REQ_V01 0x0707
#define MCM_SMS_SEND_SMS_RESP_V01 0x0707
#define MCM_SMS_ACK_SMS_REQ_V01 0x0708
#define MCM_SMS_ACK_SMS_RESP_V01 0x0708
#define MCM_SMS_GET_MEMORY_STATUS_REQ_V01 0x0709
#define MCM_SMS_GET_MEMORY_STATUS_RESP_V01 0x0709
#define MCM_SMS_SET_MEMORY_STATUS_REQ_V01 0x070A
#define MCM_SMS_SET_MEMORY_STATUS_RESP_V01 0x070A
#define MCM_SMS_GET_TRANSPORT_LAYER_INFO_REQ_V01 0x070B
#define MCM_SMS_GET_TRANSPORT_LAYER_INFO_RESP_V01 0x070B
#define MCM_SMS_GET_SERVICE_READY_STATUS_REQ_V01 0x070C
#define MCM_SMS_GET_SERVICE_READY_STATUS_RESP_V01 0x070C
#define MCM_SMS_PRIMARY_INFORMATION_IND_V01 0x070D
/**
    @}
  */


#ifdef __cplusplus
}
#endif
#endif

