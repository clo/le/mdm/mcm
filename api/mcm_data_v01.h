#ifndef MCM_DATA_SERVICE_01_H
#define MCM_DATA_SERVICE_01_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
#include <stdint.h>
#include "mcm_common_v01.h"


#ifdef __cplusplus
extern "C" {
#endif

/** @addtogroup mcm_data_consts
    @{
  */

/**  Event ID indicating DATA call is connected. */
#define MCM_DATA_NET_UP_EV_V01 0x00005001

/**  Event ID indicating DATA call is disconnected. */
#define MCM_DATA_NET_DOWN_EV_V01 0x00005002

/**  Event ID indicating a new IP address is configured for the interface. */
#define MCM_DATA_NET_NEW_ADDR_EV_V01 0x00005003

/**  Event ID indicating that one of IP address is lost. */
#define MCM_DATA_NET_DEL_ADDR_EV_V01 0x00005004

/**  Event ID indicating the current service status of modem. */
#define MCM_DATA_REG_SRVC_STATUS_EV_V01 0x00005005

/**  Event ID indicating the current bearer technology of the call. */
#define MCM_DATA_BEARER_TECH_STATUS_EV_V01 0x00005006

/**  Event ID indicating the dormancy state of the call */
#define MCM_DATA_DORMANCY_STATUS_EV_V01 0x00005007

/**  Max value of APN length */
#define MCM_DATA_MAX_APN_LEN_V01 150

/**  Max length of User name of the profile */
#define MCM_DATA_MAX_USERNAME_LEN_V01 127

/**  Max length of password */
#define MCM_DATA_MAX_PASSWORD_LEN_V01 127
#define MCM_DATA_MAX_ADDR_LEN_V01 128
#define MCM_DATA_MAX_DEVICE_NAME_LEN_V01 13
#define MCM_DATA_MAX_ADDR_COUNT_V01 10
/**
    @}
  */

/** @addtogroup mcm_data_enums
    @{
  */
typedef enum {
  MCM_DATA_CALL_STATUS_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_DATA_CALL_STATE_INVALID_V01 = 0x01, /**<  No call state  */
  MCM_DATA_CALL_STATE_CONNECTING_V01 = 0x02, /**<  Call is in activation  */
  MCM_DATA_CALL_STATE_CONNECTED_V01 = 0x03, /**<  Call state is connected  */
  MCM_DATA_CALL_STATE_DISCONNECTING_V01 = 0x04, /**<  Call is in de-activation  */
  MCM_DATA_CALL_STATE_DISCONNECTED_V01 = 0x05, /**<  Call is disconnected  */
  MCM_DATA_CALL_STATUS_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_data_call_status_t_v01;
/**
    @}
  */

/** @addtogroup mcm_data_enums
    @{
  */
typedef enum {
  MCM_DATA_SRV_STATUS_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_DATA_MODEM_STATE_OOS_V01 = 0x01, /**<  Modem is Out of service  */
  MCM_DATA_MODEM_STATE_IN_SERVICE_V01 = 0x02, /**<  Modem is in service  */
  MCM_DATA_SRV_STATUS_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_data_srv_status_t_v01;
/**
    @}
  */

/** @addtogroup mcm_data_enums
    @{
  */
typedef enum {
  MCM_DATA_BEARER_TECH_INFO_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_DATA_BEARER_TECH_TYPE_UNKNOWN_V01 = 0x00, /**<  Unknown technology    */
  MCM_DATA_BEARER_TECH_TYPE_CDMA_1X_V01 = 0x01, /**<  1X technology.         */
  MCM_DATA_BEARER_TECH_TYPE_EVDO_REV0_V01 = 0x02, /**<  CDMA Rev 0.  */
  MCM_DATA_BEARER_TECH_TYPE_EVDO_REVA_V01 = 0x03, /**<  CDMA Rev A.  */
  MCM_DATA_BEARER_TECH_TYPE_EVDO_REVB_V01 = 0x04, /**<  CDMA Rev B.  */
  MCM_DATA_BEARER_TECH_TYPE_EHRPD_V01 = 0x05, /**<  EHRPD.  */
  MCM_DATA_BEARER_TECH_TYPE_FMC_V01 = 0x06, /**<  Fixed mobile convergence.  */
  MCM_DATA_BEARER_TECH_TYPE_HRPD_V01 = 0x07, /**<  HRPD  */
  MCM_DATA_BEARER_TECH_TYPE_3GPP2_WLAN_V01 = 0x08, /**<  IWLAN  */
  MCM_DATA_BEARER_TECH_TYPE_WCDMA_V01 = 0x09, /**<  WCDMA.  */
  MCM_DATA_BEARER_TECH_TYPE_GPRS_V01 = 0x0A, /**<  GPRS.  */
  MCM_DATA_BEARER_TECH_TYPE_HSDPA_V01 = 0x0B, /**<  HSDPA.  */
  MCM_DATA_BEARER_TECH_TYPE_HSUPA_V01 = 0x0C, /**<  HSUPA.  */
  MCM_DATA_BEARER_TECH_TYPE_EDGE_V01 = 0x0D, /**<  EDGE.  */
  MCM_DATA_BEARER_TECH_TYPE_LTE_V01 = 0x0E, /**<  LTE.  */
  MCM_DATA_BEARER_TECH_TYPE_HSDPA_PLUS_V01 = 0x0F, /**<  HSDPA+.  */
  MCM_DATA_BEARER_TECH_TYPE_DC_HSDPA_PLUS_V01 = 0x10, /**<  DC HSDPA+.  */
  MCM_DATA_BEARER_TECH_TYPE_HSPA_V01 = 0x11, /**<  HSPA  */
  MCM_DATA_BEARER_TECH_TYPE_64_QAM_V01 = 0x12, /**<  64 QAM.  */
  MCM_DATA_BEARER_TECH_TYPE_TDSCDMA_V01 = 0x13, /**<  TD-SCDMA.  */
  MCM_DATA_BEARER_TECH_TYPE_GSM_V01 = 0x14, /**<  GSM  */
  MCM_DATA_BEARER_TECH_TYPE_3GPP_WLAN_V01 = 0x15, /**<  IWLAN  */
  MCM_DATA_BEARER_TECH_INFO_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_data_bearer_tech_info_t_v01;
/**
    @}
  */

/** @addtogroup mcm_data_enums
    @{
  */
typedef enum {
  MCM_DATA_DORMANCY_STATE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_DATA_DORMANCY_STATE_PHYSLINK_ACTIVE_V01 = 0, /**<  Call is Dormant  */
  MCM_DATA_DORMANCY_STATE_PHYSLINK_DORMANT_V01 = 1, /**<  Call is not Dormant  */
  MCM_DATA_DORMANCY_STATE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_data_dormancy_state_t_v01;
/**
    @}
  */

/** @addtogroup mcm_data_enums
    @{
  */
typedef enum {
  MCM_DATA_CALL_END_REASON_TYPE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_DATA_TYPE_UNSPECIFIED_V01 = 0x00,
  MCM_DATA_TYPE_MOBILE_IP_V01 = 0x01,
  MCM_DATA_TYPE_INTERNAL_V01 = 0x02,
  MCM_DATA_TYPE_CALL_MANAGER_DEFINED_V01 = 0x03,
  MCM_DATA_TYPE_3GPP_SPEC_DEFINED_V01 = 0x06,
  MCM_DATA_TYPE_PPP_V01 = 0x07,
  MCM_DATA_TYPE_EHRPD_V01 = 0x08,
  MCM_DATA_TYPE_IPV6_V01 = 0x09,
  MCM_DATA_CALL_END_REASON_TYPE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_data_call_end_reason_type_t_v01;
/**
    @}
  */

/** @addtogroup mcm_data_enums
    @{
  */
typedef enum {
  MCM_DATA_CALL_END_REASON_CODE_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_DATA_CE_INVALID_V01 = -1,
  MCM_DATA_CE_MIP_FA_ERR_REASON_UNSPECIFIED_V01 = 0,
  MCM_DATA_CE_MIP_FA_ERR_ADMINISTRATIVELY_PROHIBITED_V01,
  MCM_DATA_CE_MIP_FA_ERR_INSUFFICIENT_RESOURCES_V01,
  MCM_DATA_CE_MIP_FA_ERR_MOBILE_NODE_AUTHENTICATION_FAILURE_V01,
  MCM_DATA_CE_MIP_FA_ERR_HA_AUTHENTICATION_FAILURE_V01,
  MCM_DATA_CE_MIP_FA_ERR_REQUESTED_LIFETIME_TOO_LONG_V01,
  MCM_DATA_CE_MIP_FA_ERR_MALFORMED_REQUEST_V01,
  MCM_DATA_CE_MIP_FA_ERR_MALFORMED_REPLY_V01,
  MCM_DATA_CE_MIP_FA_ERR_ENCAPSULATION_UNAVAILABLE_V01,
  MCM_DATA_CE_MIP_FA_ERR_VJHC_UNAVAILABLE_V01,
  MCM_DATA_CE_MIP_FA_ERR_REVERSE_TUNNEL_UNAVAILABLE_V01,
  MCM_DATA_CE_MIP_FA_ERR_REVERSE_TUNNEL_IS_MANDATORY_AND_T_BIT_NOT_SET_V01,
  MCM_DATA_CE_MIP_FA_ERR_DELIVERY_STYLE_NOT_SUPPORTED_V01,
  MCM_DATA_CE_MIP_FA_ERR_MISSING_NAI_V01,
  MCM_DATA_CE_MIP_FA_ERR_MISSING_HA_V01,
  MCM_DATA_CE_MIP_FA_ERR_MISSING_HOME_ADDR_V01,
  MCM_DATA_CE_MIP_FA_ERR_UNKNOWN_CHALLENGE_V01,
  MCM_DATA_CE_MIP_FA_ERR_MISSING_CHALLENGE_V01,
  MCM_DATA_CE_MIP_FA_ERR_STALE_CHALLENGE_V01,
  MCM_DATA_CE_MIP_HA_ERR_REASON_UNSPECIFIED_V01,
  MCM_DATA_CE_MIP_HA_ERR_ADMINISTRATIVELY_PROHIBITED_V01,
  MCM_DATA_CE_MIP_HA_ERR_INSUFFICIENT_RESOURCES_V01,
  MCM_DATA_CE_MIP_HA_ERR_MOBILE_NODE_AUTHENTICATION_FAILURE_V01,
  MCM_DATA_CE_MIP_HA_ERR_FA_AUTHENTICATION_FAILURE_V01,
  MCM_DATA_CE_MIP_HA_ERR_REGISTRATION_ID_MISMATCH_V01,
  MCM_DATA_CE_MIP_HA_ERR_MALFORMED_REQUEST_V01,
  MCM_DATA_CE_MIP_HA_ERR_UNKNOWN_HA_ADDR_V01,
  MCM_DATA_CE_MIP_HA_ERR_REVERSE_TUNNEL_UNAVAILABLE_V01,
  MCM_DATA_CE_MIP_HA_ERR_REVERSE_TUNNEL_IS_MANDATORY_AND_T_BIT_NOT_SET_V01,
  MCM_DATA_CE_MIP_HA_ERR_ENCAPSULATION_UNAVAILABLE_V01,
  MCM_DATA_CE_MIP_ERR_REASON_UNKNOWN_V01,
  MCM_DATA_CE_INTERNAL_ERROR_V01,
  MCM_DATA_CE_CALL_ENDED_V01,
  MCM_DATA_CE_INTERNAL_UNKNOWN_CAUSE_CODE_V01,
  MCM_DATA_CE_UNKNOWN_CAUSE_CODE_V01,
  MCM_DATA_CE_CLOSE_IN_PROGRESS_V01,
  MCM_DATA_CE_NW_INITIATED_TERMINATION_V01,
  MCM_DATA_CE_APP_PREEMPTED_V01,
  MCM_DATA_CE_CDMA_LOCK_V01,
  MCM_DATA_CE_INTERCEPT_V01,
  MCM_DATA_CE_REORDER_V01,
  MCM_DATA_CE_REL_SO_REJ_V01,
  MCM_DATA_CE_INCOM_CALL_V01,
  MCM_DATA_CE_ALERT_STOP_V01,
  MCM_DATA_CE_ACTIVATION_V01,
  MCM_DATA_CE_MAX_ACCESS_PROBE_V01,
  MCM_DATA_CE_CCS_NOT_SUPPORTED_BY_BS_V01,
  MCM_DATA_CE_NO_RESPONSE_FROM_BS_V01,
  MCM_DATA_CE_REJECTED_BY_BS_V01,
  MCM_DATA_CE_INCOMPATIBLE_V01,
  MCM_DATA_CE_ALREADY_IN_TC_V01,
  MCM_DATA_CE_USER_CALL_ORIG_DURING_GPS_V01,
  MCM_DATA_CE_USER_CALL_ORIG_DURING_SMS_V01,
  MCM_DATA_CE_NO_CDMA_SRV_V01,
  MCM_DATA_CE_CONF_FAILED_V01,
  MCM_DATA_CE_INCOM_REJ_V01,
  MCM_DATA_CE_NO_GW_SRV_V01,
  MCM_DATA_CE_NO_GPRS_CONTEXT_V01,
  MCM_DATA_CE_ILLEGAL_MS_V01,
  MCM_DATA_CE_ILLEGAL_ME_V01,
  MCM_DATA_CE_GPRS_SERVICES_AND_NON_GPRS_SERVICES_NOT_ALLOWED_V01,
  MCM_DATA_CE_GPRS_SERVICES_NOT_ALLOWED_V01,
  MCM_DATA_CE_MS_IDENTITY_CANNOT_BE_DERIVED_BY_THE_NETWORK_V01,
  MCM_DATA_CE_IMPLICITLY_DETACHED_V01,
  MCM_DATA_CE_PLMN_NOT_ALLOWED_V01,
  MCM_DATA_CE_LA_NOT_ALLOWED_V01,
  MCM_DATA_CE_GPRS_SERVICES_NOT_ALLOWED_IN_THIS_PLMN_V01,
  MCM_DATA_CE_PDP_DUPLICATE_V01,
  MCM_DATA_CE_UE_RAT_CHANGE_V01,
  MCM_DATA_CE_CONGESTION_V01,
  MCM_DATA_CE_NO_PDP_CONTEXT_ACTIVATED_V01,
  MCM_DATA_CE_ACCESS_CLASS_DSAC_REJECTION_V01,
  MCM_DATA_CE_CD_GEN_OR_BUSY_V01,
  MCM_DATA_CE_CD_BILL_OR_AUTH_V01,
  MCM_DATA_CE_CHG_HDR_V01,
  MCM_DATA_CE_EXIT_HDR_V01,
  MCM_DATA_CE_HDR_NO_SESSION_V01,
  MCM_DATA_CE_HDR_ORIG_DURING_GPS_FIX_V01,
  MCM_DATA_CE_HDR_CS_TIMEOUT_V01,
  MCM_DATA_CE_HDR_RELEASED_BY_CM_V01,
  MCM_DATA_CE_CLIENT_END_V01,
  MCM_DATA_CE_NO_SRV_V01,
  MCM_DATA_CE_FADE_V01,
  MCM_DATA_CE_REL_NORMAL_V01,
  MCM_DATA_CE_ACC_IN_PROG_V01,
  MCM_DATA_CE_ACC_FAIL_V01,
  MCM_DATA_CE_REDIR_OR_HANDOFF_V01,
  MCM_DATA_CE_OPERATOR_DETERMINED_BARRING_V01,
  MCM_DATA_CE_LLC_SNDCP_FAILURE_V01,
  MCM_DATA_CE_INSUFFICIENT_RESOURCES_V01,
  MCM_DATA_CE_UNKNOWN_APN_V01,
  MCM_DATA_CE_UNKNOWN_PDP_V01,
  MCM_DATA_CE_AUTH_FAILED_V01,
  MCM_DATA_CE_GGSN_REJECT_V01,
  MCM_DATA_CE_ACTIVATION_REJECT_V01,
  MCM_DATA_CE_OPTION_NOT_SUPPORTED_V01,
  MCM_DATA_CE_OPTION_UNSUBSCRIBED_V01,
  MCM_DATA_CE_OPTION_TEMP_OOO_V01,
  MCM_DATA_CE_NSAPI_ALREADY_USED_V01,
  MCM_DATA_CE_REGULAR_DEACTIVATION_V01,
  MCM_DATA_CE_QOS_NOT_ACCEPTED_V01,
  MCM_DATA_CE_NETWORK_FAILURE_V01,
  MCM_DATA_CE_UMTS_REACTIVATION_REQ_V01,
  MCM_DATA_CE_FEATURE_NOT_SUPPORTED_V01,
  MCM_DATA_CE_TFT_SEMANTIC_ERROR_V01,
  MCM_DATA_CE_TFT_SYNTAX_ERROR_V01,
  MCM_DATA_CE_UNKNOWN_PDP_CONTEXT_V01,
  MCM_DATA_CE_FILTER_SEMANTIC_ERROR_V01,
  MCM_DATA_CE_FILTER_SYNTAX_ERROR_V01,
  MCM_DATA_CE_PDP_WITHOUT_ACTIVE_TFT_V01,
  MCM_DATA_CE_IP_V4_ONLY_ALLOWED_V01,
  MCM_DATA_CE_IP_V6_ONLY_ALLOWED_V01,
  MCM_DATA_CE_SINGLE_ADDR_BEARER_ONLY_V01,
  MCM_DATA_CE_INVALID_TRANSACTION_ID_V01,
  MCM_DATA_CE_MESSAGE_INCORRECT_SEMANTIC_V01,
  MCM_DATA_CE_INVALID_MANDATORY_INFO_V01,
  MCM_DATA_CE_MESSAGE_TYPE_UNSUPPORTED_V01,
  MCM_DATA_CE_MSG_TYPE_NONCOMPATIBLE_STATE_V01,
  MCM_DATA_CE_UNKNOWN_INFO_ELEMENT_V01,
  MCM_DATA_CE_CONDITIONAL_IE_ERROR_V01,
  MCM_DATA_CE_MSG_AND_PROTOCOL_STATE_UNCOMPATIBLE_V01,
  MCM_DATA_CE_PROTOCOL_ERROR_V01,
  MCM_DATA_CE_APN_TYPE_CONFLICT_V01,
  MCM_DATA_CE_PPP_TIMEOUT_V01,
  MCM_DATA_CE_PPP_AUTH_FAILURE_V01,
  MCM_DATA_CE_PPP_OPTION_MISMATCH_V01,
  MCM_DATA_CE_PPP_PAP_FAILURE_V01,
  MCM_DATA_CE_PPP_CHAP_FAILURE_V01,
  MCM_DATA_CE_PPP_UNKNOWN_V01,
  MCM_DATA_CE_EHRPD_SUBS_LIMITED_TO_V4_V01,
  MCM_DATA_CE_EHRPD_SUBS_LIMITED_TO_V6_V01,
  MCM_DATA_CE_EHRPD_VSNCP_TIMEOUT_V01,
  MCM_DATA_CE_EHRPD_VSNCP_FAILURE_V01,
  MCM_DATA_CE_EHRPD_VSNCP_3GPP2I_GEN_ERROR_V01,
  MCM_DATA_CE_EHRPD_VSNCP_3GPP2I_UNAUTH_APN_V01,
  MCM_DATA_CE_EHRPD_VSNCP_3GPP2I_PDN_LIMIT_EXCEED_V01,
  MCM_DATA_CE_EHRPD_VSNCP_3GPP2I_NO_PDN_GW_V01,
  MCM_DATA_CE_EHRPD_VSNCP_3GPP2I_PDN_GW_UNREACH_V01,
  MCM_DATA_CE_EHRPD_VSNCP_3GPP2I_PDN_GW_REJ_V01,
  MCM_DATA_CE_EHRPD_VSNCP_3GPP2I_INSUFF_PARAM_V01,
  MCM_DATA_CE_EHRPD_VSNCP_3GPP2I_RESOURCE_UNAVAIL_V01,
  MCM_DATA_CE_EHRPD_VSNCP_3GPP2I_ADMIN_PROHIBIT_V01,
  MCM_DATA_CE_EHRPD_VSNCP_3GPP2I_PDN_ID_IN_USE_V01,
  MCM_DATA_CE_EHRPD_VSNCP_3GPP2I_SUBSCR_LIMITATION_V01,
  MCM_DATA_CE_EHRPD_VSNCP_3GPP2I_PDN_EXISTS_FOR_THIS_APN_V01,
  MCM_DATA_CE_PREFIX_UNAVAILABLE_V01,
  MCM_DATA_CE_IPV6_ERR_HRPD_IPV6_DISABLED_V01,
  MCM_DATA_CALL_END_REASON_CODE_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_data_call_end_reason_code_t_v01;
/**
    @}
  */

/** @addtogroup mcm_data_aggregates
    @{
  */
typedef struct {

  mcm_data_srv_status_t_v01 srv_status;
  /**<   Identifies the Service state of modem */

  mcm_data_bearer_tech_info_t_v01 tech_info;
  /**<   Identifies the preferred technology type */
}mcm_data_reg_status_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_data_aggregates
    @{
  */
typedef struct {

  uint32_t pkts_tx;
  /**<   Number of packets transmitted.   */

  uint32_t pkts_rx;
  /**<   Number of packets received. */

  uint64_t bytes_tx;
  /**<   Number of bytes transmitted.   */

  uint64_t bytes_rx;
  /**<   Number of bytes received. */

  uint32_t pkts_dropped_tx;
  /**<   Number of transmit packets dropped. */

  uint32_t pkts_dropped_rx;
  /**<   Number of receive packets dropped. */
}mcm_data_pkt_stats_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_data_aggregates
    @{
  */
typedef struct {

  uint64_t current_tx_rate;
  /**<   Current TX data rate for the channel*/

  uint64_t current_rx_rate;
  /**<   Current RX data rate for the channel*/

  uint64_t max_tx_rate;
  /**<   Max TX data rate for the channel*/

  uint64_t max_rx_rate;
  /**<   Max RX data rate for the channel*/
}mcm_data_channel_rate_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_data_aggregates
    @{
  */
typedef struct {

  char valid_addr;
  /**<   indicates if a valid address is available or not */

  uint32_t addr_len;  /**< Must be set to # of elements in addr */
  uint8_t addr[MCM_DATA_MAX_ADDR_LEN_V01];
  /**<   stores ip address */
}mcm_data_addr_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_data_aggregates
    @{
  */
typedef struct {

  mcm_data_addr_t_v01 iface_addr_s;
  /**<   Network interface address */

  uint32_t iface_mask;
  /**<   subnet mask       */

  mcm_data_addr_t_v01 gtwy_addr_s;
  /**<   Gateway server address   */

  uint32_t gtwy_mask;
  /**<   prefix length     */

  mcm_data_addr_t_v01 dnsp_addr_s;
  /**<   Primary DNS server  address */

  mcm_data_addr_t_v01 dnss_addr_s;
  /**<   Secondary DNS server  address */
}mcm_data_addr_t_info_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_data_aggregates
    @{
  */
typedef struct {

  mcm_data_call_end_reason_type_t_v01 call_end_reason_type;
  /**<   Call end reason type. Values:
       - 0 -- Unspecified
       - 1 -- Mobile IP
       - 2 -- Internal
       - 3 -- Call Manager defined
       - 6 -- 3GPP Specification defined
       - 7 -- PPP
       - 8 -- EHRPD
       - 9 -- IPv6
  */

  mcm_data_call_end_reason_code_t_v01 call_end_reason_code;
  /**<   Reason the call ended (verbose).
  */
}mcm_data_verbose_call_end_reason_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; This method sends request to start a data call for the connection
           associated with call_id */
typedef struct {

  /* Optional */
  /*  MCM DATA IP family */
  uint8_t ip_family_valid;  /**< Must be set to true if ip_family is being passed */
  int8_t ip_family;
  /**<   IP family requested -
     4 - IPV4
     6 -IPV6
     10 - IPV4V6
  */

  /* Optional */
  /*  MCM DATA APN name */
  uint8_t apn_name_valid;  /**< Must be set to true if apn_name is being passed */
  char apn_name[MCM_DATA_MAX_APN_LEN_V01];
  /**<   APN name requested
    A charecter string that identifies a PDN as defined by the operator
  */

  /* Optional */
  /*  MCM DATA Username for APN */
  uint8_t user_name_valid;  /**< Must be set to true if user_name is being passed */
  char user_name[MCM_DATA_MAX_USERNAME_LEN_V01];
  /**<   Username for APN. */

  /* Optional */
  /*  MCM DATA Password for APN */
  uint8_t password_valid;  /**< Must be set to true if password is being passed */
  char password[MCM_DATA_MAX_PASSWORD_LEN_V01];
  /**<   Password for APN */

  /* Optional */
  /*  MCM DATA Tech preference */
  uint8_t tech_pref_valid;  /**< Must be set to true if tech_pref is being passed */
  int8_t tech_pref;
  /**<   Technology preference - UMTS or CDMA
     0 - CDMA
     1 - UMTS
  */

  /* Optional */
  /*  MCM DATA UMTS/LTE Profile */
  uint8_t umts_profile_valid;  /**< Must be set to true if umts_profile is being passed */
  int8_t umts_profile;
  /**<   Profile ID */

  /* Optional */
  /*  MCM DATA CDMA Profile */
  uint8_t cmda_profile_valid;  /**< Must be set to true if cmda_profile is being passed */
  int8_t cmda_profile;
  /**<   Profile ID */
}mcm_data_start_data_call_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; This method sends request to start a data call for the connection
           associated with call_id */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;

  /* Mandatory */
  /*  MCM DATA Call Status */
  mcm_data_call_status_t_v01 call_status;

  /* Optional */
  /*  MCM DATA Call ID */
  uint8_t call_id_valid;  /**< Must be set to true if call_id is being passed */
  int32_t call_id;
  /**<   Call ID that gets generated for a succesful call */

  /* Optional */
  /*  MCM DATA Call end reason */
  uint8_t vce_reason_valid;  /**< Must be set to true if vce_reason is being passed */
  mcm_data_verbose_call_end_reason_t_v01 vce_reason;
  /**<   Call End Reason in verbose */
}mcm_data_start_data_call_rsp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; This method sends request to stop a data call for the connection
           associated with call_id */
typedef struct {

  /* Mandatory */
  /*  MCM DATA Call ID */
  int32_t call_id;
  /**<   Call ID of the call to be stopped. */
}mcm_data_stop_data_call_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; This method sends request to stop a data call for the connection
           associated with call_id */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
}mcm_data_stop_data_call_rsp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets packet statistics for the connection associated with call_id */
typedef struct {

  /* Mandatory */
  /*  MCM DATA Call ID */
  int32_t call_id;
  /**<   Call ID of the call to be stopped. */
}mcm_data_get_pkt_stats_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets packet statistics for the connection associated with call_id */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;

  /* Optional */
  /*  Result Code */
  uint8_t pkt_stats_valid;  /**< Must be set to true if pkt_stats is being passed */
  mcm_data_pkt_stats_t_v01 pkt_stats;
}mcm_data_get_pkt_stats_rsp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Resets packet statistics for the connection associated with call_id */
typedef struct {

  /* Mandatory */
  /*  MCM DATA Call ID */
  int32_t call_id;
  /**<   Call ID of the call to be stopped. */
}mcm_data_reset_pkt_stats_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Resets packet statistics for the connection associated with call_id */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;
}mcm_data_reset_pkt_stats_rsp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets routing information for the connection associated with call_id */
typedef struct {

  /* Mandatory */
  /*  MCM DATA Call ID */
  int32_t call_id;
  /**<   Call ID of the call to be stopped. */
}mcm_data_get_device_name_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets routing information for the connection associated with call_id */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;

  /* Optional */
  /*  MCM DATA Device name */
  uint8_t device_name_valid;  /**< Must be set to true if device_name is being passed */
  char device_name[MCM_DATA_MAX_DEVICE_NAME_LEN_V01];
}mcm_data_get_device_name_rsp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets number of IP address available for the connection associated
           with call_id */
typedef struct {

  /* Mandatory */
  /*  MCM DATA Call ID */
  int32_t call_id;
  /**<   Call ID of an active call. */
}mcm_data_get_device_addr_count_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets number of IP address available for the connection associated
           with call_id */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;

  /* Optional */
  /*  MCM DATA Device addr count */
  uint8_t addr_count_valid;  /**< Must be set to true if addr_count is being passed */
  int8_t addr_count;
}mcm_data_get_device_addr_count_rsp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets underlying technology available for the connection associated
           with call_id */
typedef struct {

  /* Mandatory */
  /*  MCM DATA Call ID */
  int32_t call_id;
  /**<   Call ID of an active call. */
}mcm_data_get_call_tech_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets underlying technology available for the connection associated
           with call_id */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;

  /* Optional */
  /*  MCM DATA Call bearer tech */
  uint8_t call_tech_valid;  /**< Must be set to true if call_tech is being passed */
  mcm_data_bearer_tech_info_t_v01 call_tech;
}mcm_data_get_call_tech_rsp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets current status of the connection associated with call_id */
typedef struct {

  /* Mandatory */
  /*  MCM DATA Call ID */
  int32_t call_id;
  /**<   Call ID of an active call. */
}mcm_data_get_call_status_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets current status of the connection associated with call_id */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;

  /* Optional */
  /*  MCM DATA Call Status */
  uint8_t call_status_valid;  /**< Must be set to true if call_status is being passed */
  mcm_data_call_status_t_v01 call_status;
}mcm_data_get_call_status_rsp_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_data_get_reg_status_req_msg_v01;

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets current data registration status  */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;

  /* Optional */
  /*  MCM DATA REG Status */
  uint8_t reg_status_valid;  /**< Must be set to true if reg_status is being passed */
  mcm_data_reg_status_t_v01 reg_status;
}mcm_data_get_reg_status_rsp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets list of IP address for the connection associated with call_id */
typedef struct {

  /* Mandatory */
  /*  MCM DATA Call ID */
  int32_t call_id;
  /**<   Call ID of an active call. */
}mcm_data_get_device_addr_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets list of IP address for the connection associated with call_id */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;

  /* Optional */
  /*  MCM DATA Device addr */
  uint8_t addr_info_valid;  /**< Must be set to true if addr_info is being passed */
  uint32_t addr_info_len;  /**< Must be set to # of elements in addr_info */
  mcm_data_addr_t_info_v01 addr_info[MCM_DATA_MAX_ADDR_COUNT_V01];
}mcm_data_get_device_addr_rsp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets the current and max channel rate of connection associated with
            call_id */
typedef struct {

  /* Mandatory */
  /*  MCM DATA Call ID */
  int32_t call_id;
  /**<   Call ID of an active call. */
}mcm_data_get_channel_rate_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Gets the current and max channel rate of connection associated with
            call_id */
typedef struct {

  /* Mandatory */
  /*  Result Code */
  mcm_response_t_v01 resp;

  /* Optional */
  /*  MCM DATA Channel rate */
  uint8_t channel_rate_valid;  /**< Must be set to true if channel_rate is being passed */
  mcm_data_channel_rate_t_v01 channel_rate;
}mcm_data_get_channel_rate_rsp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Request Message; Register for Indication of Events */
typedef struct {

  /* Optional */
  uint8_t register_net_up_event_valid;  /**< Must be set to true if register_net_up_event is being passed */
  uint8_t register_net_up_event;

  /* Optional */
  uint8_t register_net_down_event_valid;  /**< Must be set to true if register_net_down_event is being passed */
  uint8_t register_net_down_event;

  /* Optional */
  uint8_t register_net_new_addr_event_valid;  /**< Must be set to true if register_net_new_addr_event is being passed */
  uint8_t register_net_new_addr_event;

  /* Optional */
  uint8_t register_net_del_addr_event_valid;  /**< Must be set to true if register_net_del_addr_event is being passed */
  uint8_t register_net_del_addr_event;

  /* Optional */
  uint8_t register_reg_srvc_status_event_valid;  /**< Must be set to true if register_reg_srvc_status_event is being passed */
  uint8_t register_reg_srvc_status_event;

  /* Optional */
  uint8_t register_bearer_tech_status_event_valid;  /**< Must be set to true if register_bearer_tech_status_event is being passed */
  uint8_t register_bearer_tech_status_event;

  /* Optional */
  uint8_t register_dormancy_status_event_valid;  /**< Must be set to true if register_dormancy_status_event is being passed */
  uint8_t register_dormancy_status_event;
}mcm_data_event_register_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Response Message; Register for Indication of Events */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_data_event_register_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_data_messages
    @{
  */
/** Indication Message; Indication corresponding to unsolicated event */
typedef struct {

  /* Mandatory */
  /*  MCM DATA Event ID */
  int32_t event_id;
  /**<   Event ID thats gets populated from one of these below
  0x00005001 -  MCM_DATA_NET_UP_EV
  0x00005002 -  MCM_DATA_NET_DOWN_EV
  0x00005003 -  MCM_DATA_NET_NEW_ADDR_EV
  0x00005004 -  MCM_DATA_NET_DEL_ADDR_EV
  0x00005005 -  MCM_DATA_REG_SRVC_STATUS_EV
  0x00005006 -  MCM_DATA_BEARER_TECH_STATUS_EV
  0x00005007 -  MCM_DATA_DORMANCY_STATUS_EV
  */

  /* Optional */
  /*  MCM DATA Call ID */
  uint8_t call_id_valid;  /**< Must be set to true if call_id is being passed */
  int32_t call_id;

  /* Optional */
  /*  Call ID that gets generated for a succesful call
 MCM DATA Call status */
  uint8_t call_status_valid;  /**< Must be set to true if call_status is being passed */
  mcm_data_call_status_t_v01 call_status;

  /* Optional */
  /*  MCM DATA Device Call bearer state */
  uint8_t call_tech_valid;  /**< Must be set to true if call_tech is being passed */
  mcm_data_bearer_tech_info_t_v01 call_tech;

  /* Optional */
  /*  MCM DATA Reg status */
  uint8_t reg_status_valid;  /**< Must be set to true if reg_status is being passed */
  mcm_data_reg_status_t_v01 reg_status;

  /* Optional */
  /*  MCM DATA Dormant status */
  uint8_t dorm_status_valid;  /**< Must be set to true if dorm_status is being passed */
  mcm_data_dormancy_state_t_v01 dorm_status;

  /* Optional */
  /*  MCM DATA Device addr count */
  uint8_t addr_count_valid;  /**< Must be set to true if addr_count is being passed */
  int8_t addr_count;

  /* Optional */
  /*  MCM DATA Device addr */
  uint8_t addr_info_valid;  /**< Must be set to true if addr_info is being passed */
  uint32_t addr_info_len;  /**< Must be set to # of elements in addr_info */
  mcm_data_addr_t_info_v01 addr_info[MCM_DATA_MAX_ADDR_COUNT_V01];

  /* Optional */
  /*  MCM DATA Call end reason */
  uint8_t vce_reason_valid;  /**< Must be set to true if vce_reason is being passed */
  mcm_data_verbose_call_end_reason_t_v01 vce_reason;
}mcm_data_unsol_event_ind_msg_v01;  /* Message */
/**
    @}
  */

/*Service Message Definition*/
/** @addtogroup mcm_data_msg_ids
    @{
  */
#define MCM_DATA_START_DATA_CALL_REQ_V01 0x0100
#define MCM_DATA_START_DATA_CALL_RSP_V01 0x0100
#define MCM_DATA_STOP_DATA_CALL_REQ_V01 0x0101
#define MCM_DATA_STOP_DATA_CALL_RSP_V01 0x0101
#define MCM_DATA_GET_PKT_STATS_REQ_V01 0x0102
#define MCM_DATA_GET_PKT_STATS_RSP_V01 0x0102
#define MCM_DATA_RESET_PKT_STATS_REQ_V01 0x0103
#define MCM_DATA_RESET_PKT_STATS_RSP_V01 0x0103
#define MCM_DATA_GET_DEVICE_NAME_REQ_V01 0x0104
#define MCM_DATA_GET_DEVICE_NAME_RSP_V01 0x0104
#define MCM_DATA_GET_DEVICE_ADDR_COUNT_REQ_V01 0x0105
#define MCM_DATA_GET_DEVICE_ADDR_COUNT_RSP_V01 0x0105
#define MCM_DATA_GET_CALL_TECH_REQ_V01 0x0106
#define MCM_DATA_GET_CALL_TECH_RSP_V01 0x0106
#define MCM_DATA_GET_CALL_STATUS_REQ_V01 0x0107
#define MCM_DATA_GET_CALL_STATUS_RSP_V01 0x0107
#define MCM_DATA_GET_DEVICE_ADDR_REQ_V01 0x0108
#define MCM_DATA_GET_DEVICE_ADDR_RSP_V01 0x0108
#define MCM_DATA_GET_CHANNEL_RATE_REQ_MSG_V01 0x0109
#define MCM_DATA_GET_CHANNEL_RATE_RSP_MSG_V01 0x0109
#define MCM_DATA_EVENT_REGISTER_REQ_V01 0x010A
#define MCM_DATA_EVENT_REGISTER_RESP_V01 0x010A
#define MCM_DATA_GET_REG_STATUS_REQ_MSG_V01 0x010B
#define MCM_DATA_GET_REG_STATUS_RSP_MSG_V01 0x010B
#define MCM_DATA_UNSOL_EVENT_IND_V01 0x010C
/**
    @}
  */

#ifdef __cplusplus
}
#endif
#endif

