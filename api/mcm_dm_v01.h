#ifndef MCM_DM_SERVICE_01_H
#define MCM_DM_SERVICE_01_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


#include <stdint.h>
#include "mcm_common_v01.h"


#ifdef __cplusplus
extern "C" {
#endif

/** @addtogroup mcm_dm_consts
    @{
  */
#define MCM_DM_STATUS_EV_V01 0x1002
#define MCM_DM_STAT_EV_V01 0x1003
#define MCM_DM_POWER_EV_V01 0x1004

/**  Max length for software version String */
#define MCM_DM_MAX_SOFTWARE_VERSION_V01 64

/**  Max length for device name string */
#define MCM_DM_MAX_DEVICE_NAME_V01 25

/**  Max array limit */
#define MCM_MAX_ARRAY_LIMIT_V01 252
/**
    @}
  */

/** @addtogroup mcm_dm_enums
    @{
  */
typedef enum {
  MCM_DM_RADIO_T_MIN_ENUM_VAL_V01 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  MCM_DM_RADIO_OFFLINE_V01 = 0x0001, /**<  Radio power off or unknown */
  MCM_DM_RADIO_ONLINE_V01 = 0x0002, /**<  Radio online */
  MCM_DM_RADIO_UNAVAILABLE_V01 = 0x0003, /**<  Radio unvailable */
  MCM_DM_RADIO_T_MAX_ENUM_VAL_V01 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}mcm_dm_radio_t_v01;
/**
    @}
  */

/** @addtogroup mcm_dm_aggregates
    @{
  */
typedef struct {

  char device[MCM_DM_MAX_DEVICE_NAME_V01 + 1];
}mcm_dm_config_parms_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_dm_aggregates
    @{
  */
typedef struct {

  uint32_t radio_power;
  /**<   current radio power level, must be one of
 mcm_dm_radio_power*/
}mcm_dm_status_parms_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_dm_aggregates
    @{
  */
typedef struct {

  char major;
  /**<   Apps software major version*/

  char minor;
  /**<   Apps software major version
 Apps build version string, null terminated*/

  char build[MCM_DM_MAX_SOFTWARE_VERSION_V01 + 1];
  /**<   device sw version string, null terminated*/

  char device[MCM_DM_MAX_SOFTWARE_VERSION_V01 + 1];
}mcm_dm_software_version_t_v01;  /* Type */
/**
    @}
  */

/** @addtogroup mcm_dm_messages
    @{
  */
/** Request Message; Configure the MCM dm interface. Configure the settings that define it. */
typedef struct {

  /* Mandatory */
  mcm_dm_config_parms_t_v01 config;
  /**<   Configuration Data Structure*/
}mcm_dm_set_config_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_dm_messages
    @{
  */
/** Response Message; Configure the MCM dm interface. Configure the settings that define it. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_dm_set_config_resp_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_dm_get_status_req_msg_v01;

/** @addtogroup mcm_dm_messages
    @{
  */
/** Response Message; Get the status associated with the device. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  mcm_dm_status_parms_t_v01 status;
  /**<   Status of the connection*/
}mcm_dm_get_status_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_dm_messages
    @{
  */
/** Request Message; Sets the device power level, eg. airplane mode. */
typedef struct {

  /* Mandatory */
  mcm_dm_radio_t_v01 radio_power;
  /**<   radio power level to set*/
}mcm_dm_set_power_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_dm_messages
    @{
  */
/** Response Message; Sets the device power level, eg. airplane mode. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t status_valid;  /**< Must be set to true if status is being passed */
  mcm_dm_status_parms_t_v01 status;
}mcm_dm_set_power_resp_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_dm_get_hardware_ver_req_msg_v01;

/** @addtogroup mcm_dm_messages
    @{
  */
/** Response Message; Returns the hardware version data. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t len_valid;  /**< Must be set to true if len is being passed */
  uint32_t len;
  /**<   bytes available for return data to be put in rev*/

  /* Optional */
  uint8_t rev_valid;  /**< Must be set to true if rev is being passed */
  char rev[MCM_MAX_ARRAY_LIMIT_V01 + 1];
  /**<   hardware revision data.*/
}mcm_dm_get_hardware_ver_resp_msg_v01;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}mcm_dm_get_software_ver_req_msg_v01;

/** @addtogroup mcm_dm_messages
    @{
  */
/** Response Message; Returns the software version data. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;

  /* Optional */
  uint8_t ver_valid;  /**< Must be set to true if ver is being passed */
  mcm_dm_software_version_t_v01 ver;
  /**<   Data structure containing both device (modem) and app
 (RIL) software version*/
}mcm_dm_get_software_ver_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_dm_messages
    @{
  */
/** Request Message; Places the device (modem) in or out of airplane mode. */
typedef struct {

  /* Mandatory */
  uint32_t airplane_mode;
  /**<   airplane_mode airplane mode on/off.
  - 1 -- on, modem is disabled, (any non-zero value).
 - 0 -- off, modem is enabled.*/
}mcm_dm_set_airplane_mode_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_dm_messages
    @{
  */
/** Response Message; Places the device (modem) in or out of airplane mode. */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_dm_set_airplane_mode_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_dm_messages
    @{
  */
/** Request Message; Register for Indication of Events */
typedef struct {

  /* Optional */
  uint8_t register_status_event_valid;  /**< Must be set to true if register_status_event is being passed */
  uint8_t register_status_event;
  /**<    - MCM_DM_STATUS_EV*/

  /* Optional */
  uint8_t register_stat_event_valid;  /**< Must be set to true if register_stat_event is being passed */
  uint8_t register_stat_event;
  /**<   - MCM_DM_STAT_EV*/

  /* Optional */
  uint8_t register_power_event_valid;  /**< Must be set to true if register_power_event is being passed */
  uint8_t register_power_event;
  /**<   - MCM_DM_POWER_EV*/
}mcm_dm_event_register_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_dm_messages
    @{
  */
/** Response Message; Register for Indication of Events */
typedef struct {

  /* Mandatory */
  mcm_response_t_v01 response;
}mcm_dm_event_register_resp_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup mcm_dm_messages
    @{
  */
/** Indication Message; Indication corresponding to unsolicated event */
typedef struct {

  /* Mandatory */
  /*  MCM DATA Event ID */
  int32_t event_id;
  /**<   Event ID thats gets populated from one of these below
  0x1002 -  MCM_DM_STATUS_EV

  0x1003 -  MCM_DM_STAT_EV

  0x1004 -  MCM_DM_POWER_EV
  */
}mcm_dm_unsol_event_ind_msg_v01;  /* Message */
/**
    @}
  */

/*Service Message Definition*/
/** @addtogroup mcm_dm_msg_ids
    @{
  */
#define MCM_DM_SET_CONFIG_REQ_V01 0x0200
#define MCM_DM_SET_CONFIG_RESP_V01 0x0200
#define MCM_DM_GET_STATUS_REQ_V01 0x0201
#define MCM_DM_GET_STATUS_RESP_V01 0x0201
#define MCM_DM_SET_POWER_REQ_V01 0x0202
#define MCM_DM_SET_POWER_RESP_V01 0x0202
#define MCM_DM_GET_HARDWARE_VER_REQ_V01 0x0203
#define MCM_DM_GET_HARDWARE_VER_RESP_V01 0x0203
#define MCM_DM_GET_SOFTWARE_VER_REQ_V01 0x0204
#define MCM_DM_GET_SOFTWARE_VER_RESP_V01 0x0204
#define MCM_DM_SET_AIRPLANE_MODE_REQ_V01 0x0205
#define MCM_DM_SET_AIRPLANE_MODE_RESP_V01 0x0205
#define MCM_DM_EVENT_REGISTER_REQ_V01 0x0206
#define MCM_DM_EVENT_REGISTER_RESP_V01 0x0206
#define MCM_DM_UNSOL_EVENT_IND_V01 0x0207
/**
    @}
  */

#ifdef __cplusplus
}
#endif
#endif

