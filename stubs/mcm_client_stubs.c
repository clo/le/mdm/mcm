#ifndef MCM_CLIENT_STUBS_H
#define MCM_CLIENT_STUBS_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
  Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

#include "mcm_client.h"

#ifdef __cplusplus
extern "C" {
#endif

uint32 mcm_client_init
(
  mcm_client_handle_type   *hndl,
  mcm_client_ind_cb         ind_cb,
  mcm_client_async_cb       default_resp_cb
)
{
  return MCM_SUCCESS_V01;
}

uint32 mcm_client_execute_command_async
(
  mcm_client_handle_type     hndl,
  int                        msg_id,
  void                      *req_c_struct,
  int                        req_c_struct_len,
  void                      *resp_c_struct,
  int                        resp_c_struct_len,
  mcm_client_async_cb        async_resp_cb,
  void                      *token_id
)
{
  return MCM_SUCCESS_V01;
}

uint32 mcm_client_execute_command_sync
(
  mcm_client_handle_type      hndl,
  int                         msg_id,
  void                       *req_c_struct,
  int                         req_c_struct_len,
  void                       *resp_c_struct,
  int                         resp_c_struct_len
)
{
  return MCM_SUCCESS_V01;
}

uint32 mcm_client_release(mcm_client_handle_type hndl)
{
  return MCM_SUCCESS_V01;
}

#ifdef __cplusplus
}
#endif

#endif // MCM_CLIENT_STUBS_H

